﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Ocelot.Middleware;
using Ocelot.Multiplexer;

namespace Task6.ApiGateway.Aggregators
{
    public class CatalogPropertiesAggregator : IDefinedAggregator
    {
        public async Task<DownstreamResponse> Aggregate(List<HttpContext> responses)
        {
            var first = await responses[0].Items.DownstreamResponse().Content.ReadAsStringAsync();
            var second = await responses[1].Items.DownstreamResponse().Content.ReadAsStringAsync();

            var contentBuilder = new StringBuilder();
            contentBuilder.Append(first);
            contentBuilder.Append(second);

            var stringContent = new StringContent(contentBuilder.ToString())
            {
                Headers = { ContentType = new MediaTypeHeaderValue("application/json") }
            };

            return new DownstreamResponse(stringContent, HttpStatusCode.OK, new List<KeyValuePair<string, IEnumerable<string>>>(), "OK");

        }
    }
}
