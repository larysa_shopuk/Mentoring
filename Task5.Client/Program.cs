﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel;
using IdentityModel.Client;

namespace Task5.Client
{
    public class Program
    {
        public const string ApiBaseUrl = "https://localhost:44307/";
        public const string IdentityServerUrl = "https://localhost:44397/";
        public const string Scope = "catalogApi";
        public const string ClientId = "catalogServiceApi.client";
        public const string ClientSecret = "SuperSecretPassword";

        public static async Task Main(string[] args)
        {
            Console.Write("Username:");
            var username = Console.ReadLine();
            Console.Write("Password:");
            var password = Console.ReadLine();

            var tokenResponse = await GetTokenAsync(username, password);
            var apiClient = new HttpClient();
            apiClient.SetBearerToken(tokenResponse.AccessToken);

            var response = await apiClient.GetAsync($"{ApiBaseUrl}api/catalog/1/properties");
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.StatusCode);
            }
            else
            {
                var content = await response.Content.ReadAsStringAsync();
                Console.WriteLine(content);
            }
        }

        private static async Task<TokenResponse> GetTokenAsync(string userName, string password)
        {
            var client = new HttpClient();
            var disco = await client.GetDiscoveryDocumentAsync(IdentityServerUrl);
            if (disco.IsError)
            {
                Console.WriteLine(disco.Error);
                return null;
            }

            var tokenResponse = await client.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = disco.TokenEndpoint,
                GrantType = OidcConstants.GrantTypes.Password,

                ClientId = ClientId,
                ClientSecret = ClientSecret,
                Scope = Scope,

                UserName = userName,
                Password = password,
                 
            });

            //var tokenResponse = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            //{
            //    Address = disco.TokenEndpoint,
            //    ClientId = ClientId,
            //    ClientSecret = ClientSecret,

            //    Scope = Scope
            //});

            if (tokenResponse.IsError)
            {
                Console.WriteLine(tokenResponse.Error);
                return null;
            }

            Console.WriteLine($"{tokenResponse.Json}\n\n");

            return tokenResponse;
        }
    }
}
