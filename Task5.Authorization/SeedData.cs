﻿using System;
using System.Linq;
using System.Security.Claims;
using IdentityModel;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using IdentityServer4.EntityFramework.Storage;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Task5.Authorization.Data;

namespace Task5.AuthorizationServer
{
    public class SeedData
    {
        public static void EnsureSeedData(string connectionString)
        {
            var assembly = typeof(SeedData).Assembly.GetName().Name;
            var services = new ServiceCollection();
            services.AddLogging();

            services.AddDbContext<AspNetIdentityDbContext>(options =>
            {
                options.UseSqlServer(connectionString, opt => opt.MigrationsAssembly(assembly));
            });

            services
                .AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<AspNetIdentityDbContext>()
                .AddDefaultTokenProviders();

            services.AddOperationalDbContext(options =>
                {
                    options.ConfigureDbContext = b =>
                        b.UseSqlServer(connectionString, opt => opt.MigrationsAssembly(assembly));
                });

            services.AddConfigurationDbContext(options =>
            {
                options.ConfigureDbContext = b =>
                    b.UseSqlServer(connectionString, opt => opt.MigrationsAssembly(assembly));
            });

            var serviceProvider = services.BuildServiceProvider();
            using var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
            using var context = scope.ServiceProvider.GetService<ConfigurationDbContext>();
            context.Database.Migrate();
            EnsureSeedData(context);

            using var ctx = scope.ServiceProvider.GetService<AspNetIdentityDbContext>();
            context.Database.Migrate();
            EnsureUsers(scope);
        }

        private static void EnsureUsers(IServiceScope scope)
        {
            var userManager = scope.ServiceProvider.GetRequiredService<UserManager<IdentityUser>>();
            var testUser = userManager.FindByNameAsync("Larysa").Result;
            if (testUser == null)
            {
                testUser = new IdentityUser
                {
                    UserName = "Larysa",
                    Email = "test@test.com",
                    EmailConfirmed = true
                };

                var result = userManager.CreateAsync(testUser, "Pass123$").Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }

                result = userManager.AddClaimsAsync(testUser, new[]
                {
                    new Claim(JwtClaimTypes.Name, "Larysa"),
                    new Claim(JwtClaimTypes.GivenName, "Larysa"),
                    new Claim(JwtClaimTypes.FamilyName, "Larysa"),
                    new Claim(JwtClaimTypes.Role, "Manager"),
                    new Claim(JwtClaimTypes.WebSite, "http://Larysa.com")
                }).Result;

                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }
            }
        }


        private static void EnsureSeedData(ConfigurationDbContext context)
        {
            if (!context.Clients.Any())
            {
                foreach (var client in Config.Clients.ToList())
                {
                    context.Clients.Add(client.ToEntity());
                }
                context.SaveChanges();
            }

            if (!context.IdentityResources.Any())
            {
                foreach (var resource in Config.IdentityResources)
                {
                    context.IdentityResources.Add(resource.ToEntity());
                }
                context.SaveChanges();
            }

            if (!context.ApiResources.Any())
            {
                foreach (var resource in Config.ApiResources)
                {
                    context.ApiResources.Add(resource.ToEntity());
                }
                context.SaveChanges();
            }

            if (!context.ApiScopes.Any())
            {
                foreach (var resource in Config.ApiScopes)
                {
                    context.ApiScopes.Add(resource.ToEntity());
                }
                context.SaveChanges();
            }
        }
    }
}
