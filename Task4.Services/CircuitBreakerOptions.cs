﻿namespace Task4.Models
{
    public class CircuitBreakerOptions
    {
        public int Attempts { get; set; }

        public int DelayInSeconds { get; set; }

        public CircuitBreakerOptions()
        {
        }
    }
}
