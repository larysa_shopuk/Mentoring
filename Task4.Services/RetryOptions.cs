﻿namespace Task4.Models
{
    public class RetryOptions
    {
        public int Attempts { get; set; }

        public int DelayInSeconds { get; set; }

        public RetryOptions()
        {
        }
    }
}
