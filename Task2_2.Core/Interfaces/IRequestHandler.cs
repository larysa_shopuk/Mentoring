﻿using System.Threading.Tasks;

namespace Task2_2.Core.Interfaces
{
    public interface IRequestHandler<TRequest, TResponse>
    {
        Task<TResponse> Handle(TRequest query);
    }
}
