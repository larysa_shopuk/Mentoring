﻿namespace Task2_2.Core.Interfaces
{
    public interface IHandlerProvider
    {
        IRequestHandler<TRequest, TResponse> ProvideHandler<TRequest, TResponse>();
    }
}
