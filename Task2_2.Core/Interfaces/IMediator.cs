﻿using System.Threading.Tasks;

namespace Task2_2.Core.Interfaces
{
    public interface IMediator
    {
        Task<TResponse> Send<TRequest, TResponse>(TRequest request);
    }
}
