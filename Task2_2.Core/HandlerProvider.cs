﻿using System;
using Task2_2.Core.Interfaces;

namespace Task2_2.Core
{
    public class HandlerProvider : IHandlerProvider
    {
        private readonly IServiceProvider _serviceProvider;

        public HandlerProvider(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IRequestHandler<TRequest, TResponse> ProvideHandler<TRequest, TResponse>()
        {
            return (IRequestHandler<TRequest, TResponse>)_serviceProvider.GetService(typeof(IRequestHandler<TRequest, TResponse>));
        }
    }
}
