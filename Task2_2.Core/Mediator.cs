﻿using System.Threading.Tasks;
using Task2_2.Core.Interfaces;

namespace Task2_2.Core
{
    public class Mediator : IMediator
    {
        private readonly IHandlerProvider _handlerProvider;

        public Mediator(IHandlerProvider handlerProvider)
        {
            _handlerProvider = handlerProvider;
        }

        public async Task<TResponse> Send<TRequest, TResponse>(TRequest request)
        {
            var handler = _handlerProvider.ProvideHandler<TRequest, TResponse>();

            return await handler.Handle(request);
        }
    }
}
