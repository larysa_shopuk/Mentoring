﻿using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Task4.Eventing.Contract;

namespace Task4.ServiceBusProcessor.Function.Contracts
{
    public interface IEventHandler
    {
        Task Handle(Event<JObject> eventMessage);
    }
}
