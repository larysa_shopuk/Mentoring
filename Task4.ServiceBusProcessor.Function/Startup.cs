﻿using System;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Task4.Models;
using Task4.ServiceBusProcessor.Function;
using Task4.ServiceBusProcessor.Function.Contracts;
using Task4.ServiceBusProcessor.Function.Models;
using Task7.Logging.Extensions;
using EventHandler = Task4.ServiceBusProcessor.Function.Handlers.EventHandler;

[assembly: FunctionsStartup(typeof(Startup))]
namespace Task4.ServiceBusProcessor.Function
{
    public class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            builder.Services
                .AddTelemetryLogging()
                .AddTransient<IEventHandler, EventHandler>()
                .AddOptions<ApiClientOptions>().Configure((Action<ApiClientOptions, IConfiguration>)((settings, configuration) => configuration.Bind("ApiClientOptions", settings))).Services
                .AddOptions<RetryOptions>().Configure((Action<RetryOptions, IConfiguration>)((settings, configuration) => configuration.Bind("RetryOptions", settings))).Services
                .AddLogging();
        }
    }
}
