﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Polly;
using Polly.Retry;
using RestSharp;
using Task4.Eventing.Contract;
using Task4.Eventing.Contract.Constants;
using Task4.Eventing.Contract.Exceptions;
using Task4.Eventing.Contract.Models;
using Task4.Models;
using Task4.ServiceBusProcessor.Function.Contracts;
using Task4.ServiceBusProcessor.Function.Models;

namespace Task4.ServiceBusProcessor.Function.Handlers
{
    public class EventHandler: IEventHandler
    {
        private const string CartId = "2";
        private readonly IOptions<ApiClientOptions> _options;
        private readonly AsyncRetryPolicy _retryPolicy;
        private readonly ILogger<EventHandler> _logger;

        public EventHandler(IOptions<ApiClientOptions> options, IOptions<RetryOptions> retryOptions, ILogger<EventHandler> logger)
        {
            _options = options;
            _logger = logger;
            _retryPolicy = Policy
                .Handle<Exception>()
                .WaitAndRetryAsync(retryOptions.Value.Attempts, retryAttempt => {
                        var timeToWait = TimeSpan.FromSeconds(retryOptions.Value.DelayInSeconds);
                        _logger.LogInformation($"Waiting {timeToWait.TotalSeconds} seconds");
                        return timeToWait;
                    }
                );
        }

        public async Task Handle(Event<JObject> eventMessage)
        {
            switch (eventMessage.EventType)
            {
                case EventTypes.ProductUpdated:
                {
                    Product product;
                    try
                    {
                        product = JsonConvert.DeserializeObject<Product>(eventMessage.Data.ToString());
                    }
                    catch
                    {
                        throw new InvalidModelException("Couldn't parse event payload");
                    }
                    var client = new RestClient(_options.Value.BaseUrl);
                    var request = new RestRequest($"/cart/{CartId}/items/{product.Id}", Method.Put)
                    {
                        RequestFormat = DataFormat.Json,
                        
                    };
                    request.AddJsonBody(product);

                    await _retryPolicy.ExecuteAsync(async () =>
                        await client.ExecuteAsync(request));

                    break;
                }

                case EventTypes.ProductDeleted:
                {
                    Product productToDelete;
                    try
                    {
                        productToDelete = JsonConvert.DeserializeObject<Product>(eventMessage.Data.ToString());
                    }
                    catch
                    {
                        throw new InvalidModelException("Couldn't parse event payload");
                    }
                    var client = new RestClient(_options.Value.BaseUrl);
                    var request = new RestRequest($"/cart/{CartId}/items/{productToDelete.Id}", Method.Delete)
                    {
                        RequestFormat = DataFormat.Json,

                    };

                    await _retryPolicy.ExecuteAsync(async () =>
                        await client.ExecuteAsync(request));
                    break;
                }

                default:
                    throw new InvalidModelException("Unexpected event type");
            }

        }
    }
}
