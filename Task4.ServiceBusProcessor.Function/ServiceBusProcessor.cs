using System;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Task4.Eventing.Contract;
using Task4.Eventing.Contract.Exceptions;
using Task4.ServiceBusProcessor.Function.Contracts;
using Task7.Logging;
using Task7.Logging.Constants;
using Task7.Logging.Extensions;

namespace Task4.ServiceBusProcessor.Function
{
    public class ServiceBusProcessor
    {
        private readonly IEventHandler _eventHandler;
        private readonly ILoggerService _loggerService;
        private readonly TelemetryClient _telemetryClient;

        public ServiceBusProcessor(IEventHandler eventHandler, ILoggerService loggerService, TelemetryConfiguration telemetryConfiguration)
        {
            _eventHandler = eventHandler;
            _loggerService = loggerService;
            _telemetryClient = new TelemetryClient(telemetryConfiguration);
        }

        [FunctionName("ServiceBusProcessor")]
        [FixedDelayRetry(2, "00:00:5")]
        public async Task Run(
            [ServiceBusTrigger(topicName: "%ServiceBusTopicName%", subscriptionName: "%ServiceBusSubscriptionName%", Connection = "ServiceBusConnectionString", AutoCompleteMessages = true)]
            ServiceBusReceivedMessage receivedMessage, ILogger log)
        {
            var operation = _telemetryClient.StartOperation<DependencyTelemetry>("Read SB message");
            operation.Telemetry.Type = "Azure SB";

            var message = receivedMessage.Body.ToString();
            var messagePayload = JsonConvert.DeserializeObject<MessagePayload>(message);
            
            try
            {
                await Process(messagePayload, log);
            }
            finally
            {
                _telemetryClient.StopOperation(operation);
            }
        }

        public async Task Process(MessagePayload messagePayload, ILogger log)
        {
            var requestTelemetry = new RequestTelemetry { Name = "Process SB message" };
            
            requestTelemetry.Context.Operation.Id = messagePayload.RootId;
            requestTelemetry.Context.Operation.ParentId = messagePayload.ParentId;
            var item = messagePayload.Payload;

            var operation = _telemetryClient.StartOperation(requestTelemetry);

            log.LogInformation($"Start to process event: {item}");
            _loggerService.LogProcessMessageEvent(EventType.MessageReceivedEvent, item);
            try
            {
                var eventData = JsonConvert.DeserializeObject<Event<JObject>>(item);

                if (string.IsNullOrEmpty(eventData?.EventType))
                {
                    var message = "Event type is not specified";
                    log.LogError(message);
                    _loggerService.LogProcessMessageEvent(EventType.MessageValidationFailedEvent, item, message);
                    throw new InvalidModelException(message);
                }

                if (eventData?.Data == null)
                {
                    var message = "Event data is not specified";
                    log.LogError(message);
                    _loggerService.LogProcessMessageEvent(EventType.MessageValidationFailedEvent, item, message);

                    throw new InvalidModelException(message);
                }

                await _eventHandler.Handle(eventData);
                _loggerService.LogProcessMessageEvent(EventType.MessageProcessingFinishedEvent, item);
            }
            catch (InvalidModelException ex)
            {
                log.LogError(ex, "Invalid event");
                _loggerService.LogProcessMessageEvent(EventType.MessageProcessingFailedEvent, item, ex.Message);
            }
            catch (Exception ex)
            {
                _loggerService.LogProcessMessageEvent(EventType.MessageProcessingFailedEvent, item, ex.Message);
                log.LogError(ex, "Failed to handle event.");
                throw;
            }
            finally
            {
                _telemetryClient.StopOperation(operation);
            }

            log.LogInformation("Processing of event finished");
        }
    }
}
