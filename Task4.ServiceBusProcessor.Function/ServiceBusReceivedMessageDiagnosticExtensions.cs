﻿using System.Diagnostics;
using Azure.Messaging.ServiceBus;

namespace Task4.ServiceBusProcessor.Function
{
    public static class ServiceBusReceivedMessageDiagnosticExtensions
    {
        public const string ActivityIdPropertyName = "Diagnostic-Id";
        public const string ActivityName = "MessageProcessing";

        public static Activity ExtractActivity(this ServiceBusReceivedMessage message, string activityName = null)
        {
            activityName ??= ActivityName;

            var activity = new Activity(activityName);

            if (TryExtractId(message, out string id))
            {
                activity.SetParentId(id);
            }

            return activity;
        }

        private static bool TryExtractId(this ServiceBusReceivedMessage message, out string id)
        {
            id = null;
            if (message.ApplicationProperties.TryGetValue(ActivityIdPropertyName, out var requestId))
            {
                if (requestId is string tmp && tmp.Trim().Length > 0)
                {
                    id = tmp;
                    return true;
                }
            }

            return false;
        }

    }
}
