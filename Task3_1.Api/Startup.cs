using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Task2_2.Application.Extensions;
using Task2_2.Core;
using Task2_2.Core.Interfaces;
using Task2_2.Infrastructure.Extensions;
using Task3_1.Api.AutoMapperProfiles;
using Task3_1.Api.Extensions;
using Task3_1.Api.Models;
using Task3_1.Api.Service;
using Task4.ServiceBusClient.Extensions;
using Task4.ServiceBusClient.Models;
using Task7.Logging.Extensions;

namespace Task3_1.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ServiceBusOptions>(Configuration.GetSection("ServiceBusConfig"));
            services.Configure<IdentityServerOptions>(Configuration.GetSection("IdentityServer"));
           
            services.AddTelemetryLogging();
            
            services.AddControllers();

            ConfigureIdentityServer(services);

            services
                .AddAutoMapper(typeof(AutoMapperProfile).Assembly, typeof(Task2_2.Application.AutoMapperProfiles.AutoMapperProfile).Assembly)
                .AddTransient<IMediator, Mediator>()
                .AddServiceBusSender()
                .AddTransient<ILinkGeneratorService, LinkGeneratorService>()
                .AddDatabase()
                .AddRouting()
                .AddQueryHandlers()
                .AddFluentValidators()
                .AddControllers();

            services.AddSwaggerGen();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Catalog API", Version = "v1" });
            });
        }

        private void ConfigureIdentityServer(IServiceCollection services)
        {
            var serviceProvider = services.BuildServiceProvider();
            var identityServerOptions = serviceProvider.GetService<IdentityServerOptions>() ?? new IdentityServerOptions();

            services.AddAuthentication("Bearer")
                .AddIdentityServerAuthentication("Bearer", options =>
                {
                    options.Authority = identityServerOptions.Url;
                    options.RequireHttpsMetadata = false;
                });
        }
        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();
            
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Catalog API v1");
                c.RoutePrefix = string.Empty;
            });

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
