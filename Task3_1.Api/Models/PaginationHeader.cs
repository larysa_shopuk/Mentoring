﻿using System.Collections.Generic;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;

namespace Task3_1.Api.Models
{
    public class PaginationHeader
    {
        public PaginationHeader(
            int currentPageNumber,
            int pageSize,
            int pagesCount,
            int itemsCount)
        {
            CurrentPageNumber = currentPageNumber;
            PageSize = pageSize;
            PagesCount = pagesCount;
            ItemsCount = itemsCount;
        }

        public int CurrentPageNumber { get; }

        public int ItemsCount { get; }

        public int PageSize { get; }

        public int PagesCount { get; }

        public string ToJsonString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public KeyValuePair<string, StringValues> ToKeyValuePair()
        {
            return new KeyValuePair<string, StringValues>("X-Pagination", ToJsonString());
        }
    }
}
