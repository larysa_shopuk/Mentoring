﻿using System.Collections.Generic;

namespace Task3_1.Api.Models
{
    public class Page<T>
    {
        public List<T> Items { get; set; } = new List<T>();

        public List<Link> Links { get; set; } = new List<Link>();
    }
}
