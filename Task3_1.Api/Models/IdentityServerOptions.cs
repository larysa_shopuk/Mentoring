﻿namespace Task3_1.Api.Models
{
    public class IdentityServerOptions
    {
        public string Url { get; set; }

        public string Scope { get; set; }

        public IdentityServerOptions()
        {
            Url = "https://localhost:44397/";
            Scope = "catalogApi";
        }
    }
}
