﻿using System;

namespace Task3_1.Api.Helpers
{
    public static class PaginationHelper
    {
        public static int GetPagesCount(int pageSize, int itemsCount)
        {
            return pageSize > 0 ? (int)Math.Ceiling(itemsCount / (double)pageSize) : 0;
        }

    }
}
