﻿using FluentValidation;
using Task3_1.Api.Contract;

namespace Task3_1.Api.Validators
{
    public class CategoryModelValidator : AbstractValidator<Category>
    {
        public CategoryModelValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .MaximumLength(50);
        }
    }
}
