﻿using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using Task3_1.Api.Contract;
using Task3_1.Api.Validators;

namespace Task3_1.Api.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddFluentValidators(this IServiceCollection services)
        {
            services.AddTransient<AbstractValidator<Category>, CategoryModelValidator>();
            services.AddTransient<AbstractValidator<Item>, ItemModelValidator>();

            return services;

        }
    }
}
