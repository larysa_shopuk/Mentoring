﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Task2_2.Application.Models;

namespace Task3_1.Api.Extensions
{
    public static class HttpResponseExtensions
    {
        public static void AddPaginationHeader<T>(this HttpResponse response, PaginatedResult<T> pageResult)
        {
            if (response is null)
                throw new ArgumentNullException(nameof(response));

            if (pageResult is null)
                throw new ArgumentNullException(nameof(pageResult));
            
            response.Headers.Add(pageResult.ToPaginationHeader().ToKeyValuePair());
        }
    }
}
