﻿using System;
using Task2_2.Application.Models;
using Task3_1.Api.Helpers;
using Task3_1.Api.Models;

namespace Task3_1.Api.Extensions
{
    public static class PaginatedResultExtensions
    {
        public static PaginationHeader ToPaginationHeader<T>(
            this PaginatedResult<T> pageResult)
        {
            if (pageResult is null)
            {
                throw new ArgumentNullException(nameof(pageResult));
            }

            return new PaginationHeader(
                pageResult.Page,
                pageResult.PageSize,
                PaginationHelper.GetPagesCount(pageResult.PageSize, pageResult.Total),
                pageResult.Total);
        }
    }
}
