﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Task2_2.Application.Exceptions;
using Task2_2.Application.Models;
using Task2_2.Application.UseCases.Items.Command.CreateItem;
using Task2_2.Application.UseCases.Items.Command.DeleteItem;
using Task2_2.Application.UseCases.Items.Command.UpdateItem;
using Task2_2.Application.UseCases.Items.Query.GetItems;
using Task2_2.Application.UseCases.Items.Query.GetItem;
using Task2_2.Core.Interfaces;
using Task3_1.Api.Contract;
using Task3_1.Api.Extensions;
using Task3_1.Api.Filters;
using Task3_1.Api.Models;
using Task3_1.Api.Service;

namespace Task3_1.Api.Controllers
{
    [ApiController]
    [Route("v1/items")]
    [TypeFilter(typeof(EntityExceptionFilter))]
    public class ItemController : ControllerBase
    {
        private readonly ILinkGeneratorService _linkGenerator;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly AbstractValidator<Item> _validator;

        public ItemController(IMediator mediator, IMapper mapper, ILinkGeneratorService linkGenerator, AbstractValidator<Item> validator)
        {
            _mediator = mediator;
            _mapper = mapper;
            _linkGenerator = linkGenerator;
            _validator = validator;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<Item>> PostItem(Item item)
        {
            var validationResult = await _validator.ValidateAsync(item);
            if (!validationResult.IsValid)
            {
                throw new InvalidModelException($"Invalid model: {string.Join("\n", validationResult.Errors)}");
            }

            var createCommand = _mapper.Map<CreateItemCommand>(item);

            var id = await _mediator.Send<CreateItemCommand, int>(createCommand);

            return CreatedAtAction(
                actionName: nameof(GetItem),
                routeValues: new { id },
                value: id);
        }

        [HttpGet("{id:int}", Name = nameof(GetItem))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<Item>> GetItem([FromRoute] int id)
        {
            var getCommand = new GetItemQuery
            {
                Id = id
            };

            var itemEntity = await _mediator.Send<GetItemQuery, Task2_2.Domain.Item>(getCommand);

            if (itemEntity == null)
            {
                return NotFound();
            }

            var item = _mapper.Map<Item>(itemEntity);


            return Ok(item);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<List<Item>>> GetItems([FromQuery] int startPage, [FromQuery] int pageSize)
        {
            var getCommand = new GetItemsQuery
            {
                PageSize = pageSize,
                StartPage = startPage
            };

            var paginatedResult = await _mediator.Send<GetItemsQuery, PaginatedResult<Task2_2.Domain.Item>>(getCommand);

            var items = _mapper.Map<List<Item>>(paginatedResult.Items);
            var page = new Page<Item>
            {
                Items = items,
                Links = _linkGenerator.CreatePaginationLinks(HttpContext, nameof(GetItems), paginatedResult.PageSize, paginatedResult.Total, paginatedResult.Page).ToList()
            };

            Response.AddPaginationHeader(paginatedResult);

            return Ok(page);
        }

        [HttpDelete("{id:int}", Name = nameof(DeleteItem))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteItem([FromRoute] int id)
        {
            var deleteCommand = new DeleteItemCommand
            {
                Id = id
            };

            await _mediator.Send<DeleteItemCommand, int>(deleteCommand);
            return NoContent();
        }

        [HttpPut("{id:int}", Name = nameof(UpdateItem))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateItem([FromRoute] int id, [FromBody] Item item)
        {
            var validationResult = await _validator.ValidateAsync(item);
            if (!validationResult.IsValid)
            {
                throw new InvalidModelException($"Invalid model: {string.Join("\n", validationResult.Errors)}");
            }

            var updateCommand = _mapper.Map<UpdateItemCommand>(item);
            updateCommand.Id = id;
            await _mediator.Send<UpdateItemCommand, Task2_2.Domain.Item>(updateCommand);

            return NoContent();
        }

       
    }
}
