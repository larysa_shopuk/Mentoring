﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Task2_2.Application.Exceptions;
using Task2_2.Application.Models;
using Task2_2.Application.UseCases.Categories.Command.CreateCategory;
using Task2_2.Application.UseCases.Categories.Command.DeleteCategory;
using Task2_2.Application.UseCases.Categories.Command.UpdateCategory;
using Task2_2.Application.UseCases.Categories.Query.GetCategories;
using Task2_2.Application.UseCases.Categories.Query.GetCategory;
using Task2_2.Core.Interfaces;
using Task3_1.Api.Contract;
using Task3_1.Api.Extensions;
using Task3_1.Api.Filters;
using Task3_1.Api.Models;
using Task3_1.Api.Service;

namespace Task3_1.Api.Controllers
{
    [ApiController]
    [Route("v1/categories")]
    [TypeFilter(typeof(EntityExceptionFilter))]
    [Authorize]
    public class CategoryController : ControllerBase
    {
        private readonly ILinkGeneratorService _linkGenerator;
        private readonly ILogger<CategoryController> _logger;
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;
        private readonly AbstractValidator<Category> _validator;

        public CategoryController(ILogger<CategoryController> logger, IMediator mediator, IMapper mapper, ILinkGeneratorService linkGenerator, AbstractValidator<Category> validator)
        {
            _logger = logger;
            _mediator = mediator;
            _mapper = mapper;
            _linkGenerator = linkGenerator;
            _validator = validator;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<Category>> PostCategory(Category category)
        {
            var validationResult = await _validator.ValidateAsync(category);
            if (!validationResult.IsValid)
            {
                throw new InvalidModelException($"Invalid model: {string.Join("\n", validationResult.Errors)}");
            }

            var createCommand = _mapper.Map<CreateCategoryCommand>(category);

            var id = await _mediator.Send<CreateCategoryCommand, int>(createCommand);

            return CreatedAtAction(
                actionName: nameof(GetCategory),
                routeValues: new { id },
                value: id);
        }

        [HttpGet("{id:int}", Name = nameof(GetCategory))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [AllowAnonymous]
        public async Task<ActionResult<Category>> GetCategory([FromRoute] int id)
        {
            var getCommand = new GetCategoryQuery
            {
                Id = id
            };

            var categoryEntity = await _mediator.Send<GetCategoryQuery, Task2_2.Domain.Category>(getCommand);

            if (categoryEntity == null)
            {
                return NotFound();
            }

            var category = _mapper.Map<Category>(categoryEntity);


            return Ok(category);
        }

        [HttpGet("{id:int}/properties", Name = nameof(GetCategoryProperties))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [AllowAnonymous]
        public async Task<ActionResult<Category>> GetCategoryProperties([FromRoute] int id)
        {
            var getCommand = new GetCategoryQuery
            {
                Id = id
            };

            var categoryEntity = await _mediator.Send<GetCategoryQuery, Task2_2.Domain.Category>(getCommand);

            if (categoryEntity == null)
            {
                return NotFound();
            }

            var properties = new Dictionary<string, string>
            {
                {nameof(categoryEntity.Name), categoryEntity.Name},
                {"Description", "Test description"}
            };

            return Ok(properties);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<List<Category>>> GetCategories([FromQuery] int startPage, [FromQuery] int pageSize)
        {
            var getCommand = new GetCategoriesQuery
            {
                PageSize = pageSize,
                StartPage = startPage
            };

            var paginatedResult = await _mediator.Send<GetCategoriesQuery, PaginatedResult<Task2_2.Domain.Category>>(getCommand);

            var categories = _mapper.Map<List<Category>>(paginatedResult.Items);
            var page = new Page<Category>
            {
                Items = categories,
                Links = _linkGenerator.CreatePaginationLinks(HttpContext, nameof(GetCategories), paginatedResult.PageSize, paginatedResult.Total, paginatedResult.Page).ToList()
            };

            Response.AddPaginationHeader(paginatedResult);

            return Ok(page);
        }

        [HttpDelete("{id:int}", Name = nameof(DeleteCategory))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteCategory([FromRoute] int id)
        {
            var deleteCommand = new DeleteCategoryCommand
            {
                Id = id
            };

            await _mediator.Send<DeleteCategoryCommand, int>(deleteCommand);
            return NoContent();
        }

        [HttpPut("{id:int}", Name = nameof(UpdateCategory))]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateCategory([FromRoute] int id, [FromBody] Category category)
        {
            var validationResult = await _validator.ValidateAsync(category);
            if (!validationResult.IsValid)
            {
                throw new InvalidModelException($"Invalid model: {string.Join("\n", validationResult.Errors)}");
            }

            var updateCommand = _mapper.Map<UpdateCategoryCommand>(category);
            updateCommand.Id = id;
            await _mediator.Send<UpdateCategoryCommand, Task2_2.Domain.Category>(updateCommand);

            return NoContent();
        }
    }
}
