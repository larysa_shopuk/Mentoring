using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.ApplicationInsights;

namespace Task3_1.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                }).ConfigureLogging(
                    (context, builder) =>
                    {
                        // Providing a connection string is required if you're using the
                        // standalone Microsoft.Extensions.Logging.ApplicationInsights package,
                        // or when you need to capture logs during application startup, such as
                        // in Program.cs or Startup.cs itself.
                        builder.AddApplicationInsights(
                            (config) =>
                                config.ConnectionString =
                                    context.Configuration["APPLICATIONINSIGHTS_CONNECTION_STRING"],
                            configureApplicationInsightsLoggerOptions: (options) => { }
                        );

                        // Capture all log-level entries from Program
                        builder.AddFilter<ApplicationInsightsLoggerProvider>(
                            typeof(Program).FullName, LogLevel.Trace);

                        // Capture all log-level entries from Startup
                        builder.AddFilter<ApplicationInsightsLoggerProvider>(
                            typeof(Startup).FullName, LogLevel.Trace);

                        builder.AddFilter<ApplicationInsightsLoggerProvider>("", LogLevel.Warning);
                    });
    }
}
