﻿using AutoMapper;
using Task2_2.Application.UseCases.Categories.Command.CreateCategory;
using Task2_2.Application.UseCases.Categories.Command.UpdateCategory;
using Task2_2.Application.UseCases.Items.Command.CreateItem;
using Task2_2.Application.UseCases.Items.Command.UpdateItem;
using Task2_2.Domain;

namespace Task3_1.Api.AutoMapperProfiles
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Contract.Category, Category>()
                .ForMember(x => x.ParentId, opt => opt.MapFrom(src => src.Parent.Id));
            CreateMap<Category, Contract.Category>().ForMember(x => x.Parent, opt => opt.MapFrom(src => src.Parent)); ;
            CreateMap<Contract.Category, CreateCategoryCommand>();
            CreateMap<Contract.Category, UpdateCategoryCommand>();

            CreateMap<Contract.Item, Item>()
                .ForMember(x => x.ParentId, opt => opt.MapFrom(src => src.Parent.Id));
            CreateMap<Item, Contract.Item>()
                .ForMember(x => x.Parent, opt => opt.MapFrom(src => src.Parent));
            CreateMap<Contract.Item, CreateItemCommand>();
            CreateMap<Contract.Item, UpdateItemCommand>();
        }
    }
}
