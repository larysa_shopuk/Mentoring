﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Task3_1.Api.Models;

namespace Task3_1.Api.Service
{
    public interface ILinkGeneratorService
    {
        IList<Link> CreatePaginationLinks(HttpContext httpContext, string actionName, int pageSize, int itemsCount, int currentPage);
    }
}
