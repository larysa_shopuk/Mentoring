﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Task3_1.Api.Helpers;
using Task3_1.Api.Models;

namespace Task3_1.Api.Service
{
    public class LinkGeneratorService : ILinkGeneratorService
    {

        private readonly LinkGenerator _linkGenerator;

        public LinkGeneratorService(LinkGenerator linkGenerator)
        {
            _linkGenerator = linkGenerator;
        }

        public IList<Link> CreatePaginationLinks(HttpContext httpContext, string actionName, int pageSize, int itemsCount, int currentPage)
        {
            var pagesCount = PaginationHelper.GetPagesCount(pageSize, itemsCount);
            var links = new List<Link>
            {
                new Link(_linkGenerator.GetUriByAction(httpContext, actionName, values: new { startPage = currentPage, pageSize }),
                    "current_page",
                    "GET"),
                new Link(_linkGenerator.GetUriByAction(httpContext, actionName, values: new { startPage = 1, pageSize }),
                    "first_page",
                    "GET"),
                new Link(_linkGenerator.GetUriByAction(httpContext, actionName, values: new { startPage = pagesCount != 0 ? pagesCount : 1, pageSize }),
                    "last_page",
                    "GET")
            };

            if (itemsCount == 0)
            {
                return links;
            }

            if (currentPage > 1)
            {
                links.Add(new Link(_linkGenerator.GetUriByAction(httpContext, actionName, values: new { startPage = currentPage - 1, pageSize }),
                    "previous_page",
                    "GET"));
            }

            if (currentPage < pagesCount)
            {
                links.Add(new Link(_linkGenerator.GetUriByAction(httpContext, actionName, values: new { startPage = currentPage + 1, pageSize }),
                    "next_page",
                    "GET"));
            }

            return links;
        }
    }
}
