﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Newtonsoft.Json;
using Task2_1.BLL.Contracts;
using Task2_1.BLL.Models;
using Task2_1.DAL.Contracts;
using Task7.Logging;
using Task7.Logging.Constants;
using Task7.Logging.Extensions;

namespace Task2_1.BLL
{
    public class CartService : ICartService
    {
        private readonly ICartRepository _cartRepository;
        private readonly IMapper _mapper;
        private readonly ILoggerService  _loggerService;

        public CartService(ICartRepository cartRepository, IMapper mapper, ILoggerService loggerService)
        {
            _cartRepository = cartRepository;
            _mapper = mapper;
            _loggerService = loggerService;
        }

        public int AddItem(Item item)
        {
            try
            {
                var itemEntity = _mapper.Map<DAL.Models.Item>(item);
                var itemId = _cartRepository.AddItem(itemEntity);
                _loggerService.LogAddItemEvent(itemId, Status.Success, ApplicationType.CartApi);

                return itemId;
            }
            catch (Exception ex)
            {
                _loggerService.LogAddItemEvent(null, Status.Failure, $"Failed to add the following item {JsonConvert.SerializeObject(item)}. Reason: {ex.Message}");
                throw;
            }
        }

        public void RemoveItem(int itemId)
        {
            try
            {
                _cartRepository.RemoveItem(itemId);
                _loggerService.LogDeleteItemEvent(itemId, Status.Success, ApplicationType.CartApi);
            }
            catch (Exception ex)
            {
                _loggerService.LogDeleteItemEvent(itemId, Status.Failure, ex.Message);
                throw;
            }
        }

        public void UpdateItem(int itemId, Item item)
        {
            try
            {
                var itemEntity = _mapper.Map<DAL.Models.Item>(item);
                _cartRepository.UpdateItem(itemId, itemEntity);
                _loggerService.LogUpdateItemEvent(itemId, Status.Success, ApplicationType.CartApi);
            }
            catch (Exception ex)
            {
                _loggerService.LogUpdateItemEvent(itemId, Status.Failure, ex.Message);
                throw;
            }
        }

        public IList<Item> GetItems(int startPage = 0, int pageSize = 10)
        {
            var itemEntities = _cartRepository.GetAll(startPage, pageSize);

            return _mapper.Map<IList<Item>>(itemEntities);
        }
    }
}
