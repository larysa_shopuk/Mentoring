﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Task2_1.BLL.Contracts;
using Task2_1.DAL.Contracts;
using Task2_1.DAL.Repositories;

namespace Task2_1.BLL.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddService(this IServiceCollection services)
        {
            var serviceProvider = services.BuildServiceProvider();
            var configuration = serviceProvider.GetService<IConfiguration>();
            var connectionString = configuration.GetConnectionString("DbContext");
            var carRepository = new CartRepository(connectionString);
            
            services.AddSingleton<ICartRepository>(carRepository);
            services.AddTransient<ICartService, CartService>();

            return services;
        }
    }
}
