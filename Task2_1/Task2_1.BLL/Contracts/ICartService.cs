﻿using System.Collections.Generic;
using Task2_1.BLL.Models;

namespace Task2_1.BLL.Contracts
{
    public interface ICartService
    {
        int AddItem(Item item);

        void RemoveItem(int itemId);

        void UpdateItem(int itemId, Item item);

        IList<Item> GetItems(int startPage = 0, int pageSize = 10);
    }
}
