﻿namespace Task2_1.BLL.Models
{
    public class ItemBase
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }

        public decimal Price { get; set; }
    }
}
