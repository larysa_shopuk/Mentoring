﻿using AutoMapper;
using Task2_1.BLL.Models;

namespace Task2_1.BLL.AutoMapperProfiles
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Item, DAL.Models.Item>();
            CreateMap<DAL.Models.Item, Item>();
        }
    }
}
