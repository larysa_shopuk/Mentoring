﻿using AutoMapper;
using Task3_2.Api.Models;

namespace Task3_2.Api.AutoMapperProfiles
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Item, Task2_1.BLL.Models.Item>();
            CreateMap<Task2_1.BLL.Models.Item, Item>();
        }
    }
}
