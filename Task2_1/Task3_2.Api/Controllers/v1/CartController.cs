﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Task2_1.BLL.Contracts;
using Task3_2.Api.Filters;
using Task3_2.Api.Models;
using Task4.Eventing.Contract.Exceptions;

namespace Task3_2.Api.Controllers.v1
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("v{version:apiVersion}/cart/{cartId}/items")]
    [TypeFilter(typeof(EntityExceptionFilter))]
    public class CartController : ControllerBase
    {
        private readonly ICartService _cartService;
        private readonly IMapper _mapper;
        private readonly AbstractValidator<Item> _validator;
        private readonly ILogger<CartController> _logger;

        public CartController(ICartService cartService, IMapper mapper, AbstractValidator<Item> validator, ILogger<CartController> logger)
        {
            _cartService = cartService;
            _mapper = mapper;
            _validator = validator;
            _logger = logger;
        }

        [HttpGet]
        [MapToApiVersion("1.0")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<Cart> GetCartInfo([FromRoute] string cartId)
        {
            var cartItems = _cartService.GetItems();

            var cartInfo = new Cart
            {
                CartId = cartId,
                Items = _mapper.Map<IList<Item>>(cartItems)
            };

            return Ok(cartInfo);
        }

        [HttpPost]
        [MapToApiVersion("1.0")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<Cart>> AddItem([FromRoute] string cartId, [FromBody] Item item)
        {
            using var scope = _logger.BeginScope("CartController:AddItem");
            var validationResult = await _validator.ValidateAsync(item);
            if (!validationResult.IsValid)
            {
                throw new InvalidModelException($"Invalid model: {string.Join("\n", validationResult.Errors)}");
            }

            var itemToCreate = _mapper.Map<Task2_1.BLL.Models.Item>(item);
            _logger.LogInformation("Try to add new product");
            var id = _cartService.AddItem(itemToCreate);

            return Ok();
        }

        [HttpDelete("{id}", Name = nameof(DeleteItem))]
        [MapToApiVersion("1.0")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<Cart> DeleteItem([FromRoute] string cartId, [FromRoute] int id)
        {
            using var scope = _logger.BeginScope("CartController:RemoveItem");

            _logger.LogInformation($"Try to delete '{id}' product");
            _cartService.RemoveItem(id);

            return Ok();
        }

        [HttpPut("{id}", Name = nameof(UpdateItem))]
        [MapToApiVersion("1.0")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<Cart> UpdateItem([FromRoute] string cartId, [FromRoute] int id, [FromBody] Item item)
        {
            using var scope = _logger.BeginScope("CartController:UpdateItem");
            var itemToUpdate = _mapper.Map<Task2_1.BLL.Models.Item>(item);
            _logger.LogInformation($"Try to update details for '{id}' product");
            _cartService.UpdateItem(id, itemToUpdate);

            return Ok();
        }
    }
}
