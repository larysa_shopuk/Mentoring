﻿using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Task2_1.BLL.Contracts;
using Task3_2.Api.Models;

namespace Task3_2.Api.Controllers.v2
{
    [ApiController]
    [ApiVersion("2.0")]
    [Route("v{version:apiVersion}/cart/{cartId}/items")]
    public class CartController : ControllerBase
    {
        private readonly ICartService _cartService;
        private readonly ILogger<CartController> _logger;
        private readonly IMapper _mapper;

        public CartController(ILogger<CartController> logger, ICartService cartService, IMapper mapper)
        {
            _logger = logger;
            _cartService = cartService;
            _mapper = mapper;
        }

        [HttpGet]
        [MapToApiVersion("2.0")]
        public ActionResult<IList<Item>> GetCartItems([FromRoute] string cartId)
        {
            var cartItems = _cartService.GetItems();
            
            return Ok(_mapper.Map<IList<Item>>(cartItems));
        }
    }
}
