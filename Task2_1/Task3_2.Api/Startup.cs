using FluentValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Task2_1.BLL.Extensions;
using Task3_2.Api.AutoMapperProfiles;
using Task3_2.Api.Extensions;
using Task3_2.Api.Models;
using Task3_2.Api.Validators;
using Task4.ServiceBusClient.Extensions;
using Task4.ServiceBusClient.Models;
using Task7.Logging.Extensions;

namespace Task3_2.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ServiceBusOptions>(Configuration.GetSection("ServiceBusConfig"));
            services.AddTelemetryLogging();

            services
                .AddAutoMapper(typeof(AutoMapperProfile).Assembly, typeof(Task2_1.BLL.AutoMapperProfiles.AutoMapperProfile).Assembly)
                .AddService()
                .AddServiceBusSender()
                .AddSwagger()
                .AddVersioning()
                .AddTransient<AbstractValidator<Item>, ItemModelValidator>()
                .AddControllers();

            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IApiVersionDescriptionProvider provider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                foreach (var description in provider.ApiVersionDescriptions)
                {
                    options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.GroupName);
                }

                options.RoutePrefix = string.Empty;
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
