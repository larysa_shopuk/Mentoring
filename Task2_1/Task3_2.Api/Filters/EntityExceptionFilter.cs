﻿using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Task2_1.DAL.Exceptions;
using Task4.Eventing.Contract.Exceptions;

namespace Task3_2.Api.Filters
{
    public class EntityExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            switch (context.Exception)
            {
                case EntityNotFoundException _:
                    context.Result = new ContentResult
                    {
                        Content = context.Exception.Message,
                        StatusCode = (int)HttpStatusCode.NotFound
                    };

                    break;

                case InvalidModelException _:
                    context.Result = new ContentResult
                    {
                        Content = context.Exception.Message,
                        StatusCode = (int)HttpStatusCode.BadRequest
                    };

                    break;
                
                case DuplicateEntityException _:
                    context.Result = new ContentResult
                    {
                        Content = context.Exception.Message,
                        StatusCode = (int)HttpStatusCode.UnprocessableEntity
                    };

                    break;
            }
        }
    }
}