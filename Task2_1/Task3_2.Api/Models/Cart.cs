﻿using System.Collections.Generic;

namespace Task3_2.Api.Models
{
    public class Cart
    {
        public string CartId { get; set; }

        public IList<Item> Items { get; set; }
    }
}
