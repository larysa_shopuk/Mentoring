﻿using FluentValidation;
using Task3_2.Api.Models;

namespace Task3_2.Api.Validators
{
    public class ItemModelValidator : AbstractValidator<Item>
    {
        public ItemModelValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(x => x.Description)
                .MaximumLength(255);

            RuleFor(x => x.Image)
                .MaximumLength(255);
        }

    }
}
