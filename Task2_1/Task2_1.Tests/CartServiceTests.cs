﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Task2_1.BLL;
using Task2_1.BLL.AutoMapperProfiles;
using Task2_1.BLL.Contracts;
using Task2_1.BLL.Extensions;
using Task2_1.DAL.Contracts;
using Task2_1.DAL.Models;
using Xunit;

namespace Task2_1.Tests
{
    public class CartServiceTests
    {
        private readonly Mock<ICartRepository> _repositoryMock;
        private readonly IServiceCollection _serviceCollection;
        private readonly IFixture _fixture;

        public CartServiceTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            _repositoryMock = new Mock<ICartRepository>();
            _serviceCollection = new ServiceCollection().AddAutoMapper(typeof(AutoMapperProfile).Assembly);
            _serviceCollection.AddSingleton(p => _repositoryMock.Object);
            _serviceCollection.AddTransient<ICartService, CartService>();
        }

        [Fact]
        public async Task GivenGetItems_WhenCalled_ThenRepositoryCallResultIsReturned()
        {
            await using var provider = _serviceCollection.BuildServiceProvider();
            var items = _fixture.CreateMany<Item>(5).ToArray();
            const int startPage = 0;
            const int pageSize = 5;
            _repositoryMock.Setup(x => x.GetAll(startPage, pageSize)).Returns(items);

            var service = provider.GetService<ICartService>();

            var result = service.GetItems(startPage, pageSize);

            Assert.Equal(items.Length, result.Count);

            _repositoryMock.Verify(x => x.GetAll(startPage, pageSize), Times.Once);
        }

        [Fact]
        public async Task GivenAddItem_WhenCalled_ThenExpectedRepositoryMethodCalled()
        {
            await using var provider = _serviceCollection.BuildServiceProvider();
            var id = 1;
            var item = _fixture.Create<BLL.Models.Item>();
            _repositoryMock.Setup(x => x.AddItem(It.IsAny<Item>())).Returns(id);

            var service = provider.GetService<ICartService>();

            var result = service.AddItem(item);

            Assert.Equal(id, result);

            _repositoryMock.Verify(x => x.AddItem(It.IsAny<Item>()), Times.Once);
        }

        [Fact]
        public async Task GivenRemoveItem_WhenCalled_ThenExpectedRepositoryMethodCalled()
        {
            await using var provider = _serviceCollection.BuildServiceProvider();
            var id = 1;
            _repositoryMock.Setup(x => x.RemoveItem(id));

            var service = provider.GetService<ICartService>();

            service.RemoveItem(id);

            _repositoryMock.Verify(x => x.RemoveItem(id), Times.Once);
        }
    }
}
