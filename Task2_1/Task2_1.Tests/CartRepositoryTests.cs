using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using LiteDB;
using Task2_1.DAL.Constants;
using Task2_1.DAL.Contracts;
using Task2_1.DAL.Exceptions;
using Task2_1.DAL.Models;
using Task2_1.DAL.Repositories;
using Xunit;

namespace Task2_1.Tests
{
    public class CartRepositoryTests
    {
        public const string DbConnectionString = "C:\\Temp\\Task2_1_Database.db";
        public const int InitialItemsCount = 5;
        private readonly IFixture _fixture;
        private readonly ICartRepository _repository;

        public CartRepositoryTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _fixture.Behaviors.Add(new OmitOnRecursionBehavior());

            _repository = new CartRepository(DbConnectionString);

            SeedDb();
        }

        [Fact]
        public void GivenAddItem_WhenCalled_ThenItemShouldBeCreatedInDb()
        {
            var newItem = _fixture.Create<Item>();

            var repository = new CartRepository(DbConnectionString);
            var createdItemId = repository.AddItem(newItem);

            using var db = new LiteDatabase(DbConnectionString);
            var itemCollection = db.GetCollection<Item>(Entity.Items);
            var createdItem = itemCollection.Query().Where(x => x.ItemId == createdItemId).FirstOrDefault();
            Assert.NotNull(createdItem);

            itemCollection.Delete(createdItemId);
        }

        [Fact]
        public void GivenRemoveItem_WhenCalled_ThenItemShouldBeRemovedFromDb()
        {
            var id = 1;

            using (var db = new LiteDatabase(DbConnectionString))
            {
                var itemCollection = db.GetCollection<Item>(Entity.Items);
                var newItem = _fixture.Build<Item>().With(x => x.ItemId, id).Create();
                itemCollection.Insert(newItem);
                var itemToDelete = itemCollection.Query().Where(x => x.ItemId == id).FirstOrDefault();
                Assert.NotNull(itemToDelete);
            }

            var repository = new CartRepository(DbConnectionString);
            repository.RemoveItem(id);

            using (var db = new LiteDatabase(DbConnectionString))
            {
                var itemCollection = db.GetCollection<Item>(Entity.Items);
                var deletedItem = itemCollection.Query().Where(x => x.ItemId == id).FirstOrDefault();
                Assert.Null(deletedItem);
            }
        }

        [Fact]
        public void GivenRemoveItem_WhenCalledAndNoItem_ThenExceptionShouldBeThrown()
        {
            var id = 1;

            var repository = new CartRepository(DbConnectionString);

            var exception = Assert.Throws<EntityNotFoundException>(() => repository.RemoveItem(id));

            Assert.Equal("Entity not found", exception.Message);
        }

        [Fact]
        public void GivenGetAll_WhenCalledWithExistingPageNumber_ThenItemsShouldBeReturned()
        {
            var actualItems = _repository.GetAll();

            Assert.Equal(InitialItemsCount, actualItems.Count());
        }

        [Fact]
        public void GivenGetAll_WhenCalledWithNotExistingPageNumber_ThenNoItemsShouldBeReturned()
        {
            var actualItems = _repository.GetAll(20);

            Assert.Empty(actualItems);
        }

        private void SeedDb()
        {
            using var db = new LiteDatabase(DbConnectionString);
            db.DropCollection(Entity.Items);
            var itemCollection = db.GetCollection<Item>(Entity.Items);
            var items = _fixture.CreateMany<Item>(InitialItemsCount).ToArray();
            itemCollection.InsertBulk(items);
        }
    }


}
