﻿using System;

namespace Task2_1.DAL.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException() : base("Entity not found")
        {
        }
    }
}