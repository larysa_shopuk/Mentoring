﻿using System;

namespace Task2_1.DAL.Exceptions
{
    public class DuplicateEntityException : Exception
    {
        public DuplicateEntityException() : base("Entity already exists")
        {
        }
    }
}