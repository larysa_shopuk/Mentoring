﻿using System.Collections.Generic;
using Task2_1.DAL.Models;

namespace Task2_1.DAL.Contracts
{
    public interface ICartRepository
    {
        int AddItem(Item item);
        
        void RemoveItem(int itemId);

        void UpdateItem(int itemId, Item item);

        IEnumerable<Item>  GetAll(int startPage = 0, int pageSize = 10);
    }
}
