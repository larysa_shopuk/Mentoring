﻿using System.Collections.Generic;
using AutoMapper;
using LiteDB;
using Task2_1.DAL.Constants;
using Task2_1.DAL.Contracts;
using Task2_1.DAL.Exceptions;
using Task2_1.DAL.Models;

namespace Task2_1.DAL.Repositories
{
    public class CartRepository : ICartRepository
    {
        public readonly string DbConnectionString;
        private readonly IMapper _mapper;

        public CartRepository(string dbConnectionString)
        {
            DbConnectionString = dbConnectionString;
            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<DAL.Models.Item, DAL.Models.Item>();
            }));
        }

        public int AddItem(Item item)
        {
            using var db = new LiteDatabase(DbConnectionString);
            var itemCollection = db.GetCollection<Item>(Entity.Items);
            var existing = itemCollection.Query()
                .Where(x => x.Name == item.Name && x.Image == item.Image && x.Description == item.Description)
                .FirstOrDefault();
            if (existing != null)
            {
                throw new DuplicateEntityException();
            }

            var lastItemId = itemCollection.Query().Count();

            item.ItemId = lastItemId + 1;
            itemCollection.Insert(item);

            return item.ItemId;
        }

        public void RemoveItem(int itemId)
        {
            using var db = new LiteDatabase(DbConnectionString);
            var itemCollection = db.GetCollection<Item>(Entity.Items);
            if (!itemCollection.Exists(Query.EQ("_id", itemId)))
            {
                throw new EntityNotFoundException();
            }

            itemCollection.Delete(itemId);
        }

        public void UpdateItem(int itemId, Item item)
        {
            using var db = new LiteDatabase(DbConnectionString);
            var itemCollection = db.GetCollection<Item>(Entity.Items);
            var existing = itemCollection.Query()
                .Where(x => x.ItemId == itemId)
                .FirstOrDefault();
            if (existing == null)
            {
                throw new EntityNotFoundException();
            }

            var updatedItem = _mapper.Map(item, existing);
            updatedItem.ItemId = itemId;

            itemCollection.Update(updatedItem);
        }

        public IEnumerable<Item> GetAll(int startPage = 0, int pageSize = 10)
        {
            using var db = new LiteDatabase(DbConnectionString);
            var itemCollection = db.GetCollection<Item>(Entity.Items);
            var offset = startPage * pageSize;
            var results = itemCollection.Query()
                .OrderBy(x => x.Name)
                .Skip(offset)
                .Limit(pageSize)
                .ToList();

            return results;
        }
    }
}
