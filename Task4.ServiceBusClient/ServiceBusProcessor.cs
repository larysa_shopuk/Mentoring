﻿using System;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using Microsoft.Extensions.Options;
using Task4.ServiceBusClient.Contracts;
using Task4.ServiceBusClient.Models;

namespace Task4.ServiceBusClient
{
    public class ServiceBusProcessor : IServiceBusProcessor, IDisposable
    {
        private readonly Azure.Messaging.ServiceBus.ServiceBusClient _client;
        private readonly IOptions<ServiceBusOptions> _options;
        private Azure.Messaging.ServiceBus.ServiceBusProcessor _processor;
        private bool _disposed;

        public ServiceBusProcessor(IOptions<ServiceBusOptions> options)
        {
            _client = new Azure.Messaging.ServiceBus.ServiceBusClient(options.Value.ConnectionString);
            _options = options;
        }

        public async Task Subscribe(Func<ProcessMessageEventArgs, Task> messageHandler, Func<ProcessErrorEventArgs, Task> errorHandler)
        {
            _processor = _client.CreateProcessor(_options.Value.TopicName, _options.Value.SubscriptionName, new ServiceBusProcessorOptions());

            _processor.ProcessMessageAsync += messageHandler;
            _processor.ProcessErrorAsync += errorHandler;
            
            await _processor.StartProcessingAsync();
        }

        public async Task Unsubscribe()
        {
            try
            {
                await _processor.StopProcessingAsync();
            }
            finally
            {
                await _processor.DisposeAsync();
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        protected virtual async Task Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    await Unsubscribe();
                    await _client.DisposeAsync();
                }

                _disposed = true;
            }
        }
    }
}
