﻿namespace Task4.ServiceBusClient.Models
{
    public class ServiceBusOptions
    {
        public string ConnectionString { get; set; }

        public string TopicName { get; set; }

        public string SubscriptionName { get; set; }

        public ServiceBusOptions()
        {
        }
    }
}
