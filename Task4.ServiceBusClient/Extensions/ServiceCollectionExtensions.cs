﻿using Microsoft.Extensions.DependencyInjection;
using Task4.ServiceBusClient.Contracts;

namespace Task4.ServiceBusClient.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServiceBusSender(this IServiceCollection services)
        {
            services.AddTransient<IServiceBusSender, ServiceBusSender>();

            return services;

        }

        public static IServiceCollection AddServiceBusProcessor(this IServiceCollection services)
        {
            services.AddTransient<IServiceBusProcessor, ServiceBusProcessor>();

            return services;

        }
    }
}
