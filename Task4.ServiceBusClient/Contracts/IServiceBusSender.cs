﻿using System.Threading.Tasks;

namespace Task4.ServiceBusClient.Contracts
{
    public interface IServiceBusSender
    {
        Task SendMessage(string message);
    }
}
