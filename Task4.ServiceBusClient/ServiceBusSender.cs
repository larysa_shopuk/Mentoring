﻿using System;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Task4.Eventing.Contract;
using Task4.ServiceBusClient.Contracts;
using Task4.ServiceBusClient.Models;

namespace Task4.ServiceBusClient
{
    public class ServiceBusSender : IServiceBusSender, IDisposable
    {
        private readonly Azure.Messaging.ServiceBus.ServiceBusClient _client;
        private readonly IOptions<ServiceBusOptions> _options;
        private readonly TelemetryClient _telemetryClient;
        private bool _disposed = false;

        public ServiceBusSender(IOptions<ServiceBusOptions> options, TelemetryConfiguration telemetryConfiguration)
        {
            _client = new Azure.Messaging.ServiceBus.ServiceBusClient(options.Value.ConnectionString);
            _options = options;
            _telemetryClient = new TelemetryClient(telemetryConfiguration);
        }

        public async Task SendMessage(string message)
        {
            var operation = _telemetryClient.StartOperation<DependencyTelemetry>("Send " + _options.Value.TopicName);
            operation.Telemetry.Type = "Azure SB";
            operation.Telemetry.Data = message;

            var jsonPayload = JsonConvert.SerializeObject(new MessagePayload
            {
                RootId = operation.Telemetry.Context.Operation.Id,
                ParentId = operation.Telemetry.Id,
                Payload = message
            });

            var sender = _client.CreateSender(_options.Value.TopicName);
            using ServiceBusMessageBatch messageBatch = await sender.CreateMessageBatchAsync();

            if (!messageBatch.TryAddMessage(new ServiceBusMessage(jsonPayload)))
            {
                throw new Exception("The message is too large to fit in the batch.");
            }

            try
            {
                await sender.SendMessagesAsync(messageBatch);
                Console.WriteLine("A message has been published.");
            }
            catch (Exception e)
            {
                operation.Telemetry.Success = false;
                _telemetryClient.TrackException(e);
                throw;
            }
            finally
            {
                _telemetryClient.StopOperation(operation);
                await sender.DisposeAsync();
            }
        }
        
        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                   _client.DisposeAsync();
                }

                _disposed = true;
            }
        }
    }
}
