﻿namespace Task3_1.Api.Contract
{
    public class Category
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Image { get; set; }

        public Category Parent { get; set; }
    }
}
