﻿namespace Task3_1.Api.Contract
{
    public class Item
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }

        public int Amount { get; set; }

        public decimal Price { get; set; }

        public Category Parent { get; set; }
    }
}
