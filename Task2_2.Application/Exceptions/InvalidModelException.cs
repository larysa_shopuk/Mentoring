﻿using System;

namespace Task2_2.Application.Exceptions
{
    public class InvalidModelException : Exception
    {
        public InvalidModelException(string message) : base(message)
        {
        }
    }
}