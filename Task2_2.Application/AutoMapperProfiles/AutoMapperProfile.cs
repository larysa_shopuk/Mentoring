﻿using AutoMapper;
using Task2_2.Application.UseCases.Categories.Command.CreateCategory;
using Task2_2.Application.UseCases.Categories.Command.UpdateCategory;
using Task2_2.Application.UseCases.Items.Command.CreateItem;
using Task2_2.Application.UseCases.Items.Command.UpdateItem;
using Task2_2.Domain;

namespace Task2_2.Application.AutoMapperProfiles
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<CreateCategoryCommand, Category>();
            CreateMap<UpdateCategoryCommand, Category>();
            CreateMap<Category, Category>()
                .ForMember(x => x.Updated, opt => opt.Ignore())
                .ForMember(x => x.Created, opt => opt.Ignore());

            CreateMap<CreateItemCommand, Item>();
            CreateMap<UpdateItemCommand, Item>();
            CreateMap<Item, Item>()
                .ForMember(x => x.Updated, opt => opt.Ignore())
                .ForMember(x => x.Created, opt => opt.Ignore());
        }
    }
}
