﻿using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Task2_2.Core;
using Task2_2.Core.Interfaces;

namespace Task2_2.Application.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddQueryHandlers(this IServiceCollection services)
        {
            services.AddTransient<IMediator, Mediator>();
            services.AddTransient<IHandlerProvider, HandlerProvider>(provider => new HandlerProvider(provider));

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            var allHandlers = assemblies.SelectMany(s => s.GetTypes())
                .Where(handler => !handler.IsAbstract && handler.GetInterface(typeof(IRequestHandler<,>).Name) != null);
            foreach (var handler in allHandlers)
            {
                var interfaceType = handler.GetInterfaces().FirstOrDefault();
                services.AddTransient(interfaceType, handler);
            }

            return services;
        }
    }
}
