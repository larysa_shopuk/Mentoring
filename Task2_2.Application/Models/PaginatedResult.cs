﻿using System.Collections.Generic;

namespace Task2_2.Application.Models
{
    public class PaginatedResult<T>
    {
        public IList<T> Items { get; set; }

        public int Page { get; set; }

        public int PageSize { get; set; }

        public int Total { get; set; }
    }
}
