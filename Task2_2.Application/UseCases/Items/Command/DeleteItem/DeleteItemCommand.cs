﻿using Task2_2.Application.Interfaces;

namespace Task2_2.Application.UseCases.Items.Command.DeleteItem
{
    public class DeleteItemCommand : IRequest<int>
    {
        public int Id { get; set; }
    }
}
