﻿using System;
using System.Threading.Tasks;
using Microsoft.ApplicationInsights;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Task2_2.Application.Exceptions;
using Task2_2.Application.Interfaces;
using Task2_2.Core.Interfaces;
using Task2_2.Domain;
using Task4.Eventing.Contract;
using Task4.Eventing.Contract.Constants;
using Task4.ServiceBusClient.Contracts;
using Task7.Logging;
using Task7.Logging.Constants;
using Task7.Logging.Extensions;

namespace Task2_2.Application.UseCases.Items.Command.DeleteItem
{
    public class DeleteItemCommandHandler : IRequestHandler<DeleteItemCommand, int>
    {
        protected readonly IContext _context;
        private readonly IServiceBusSender _serviceBusService;
        private readonly ILoggerService _loggerService;
        private readonly TelemetryClient _telemetryClient;

        public DeleteItemCommandHandler(IContext context, IServiceBusSender serviceBusService, ILoggerService loggerService)
        {
            _context = context;
            _serviceBusService = serviceBusService;
            _loggerService = loggerService;
        }

        public async Task<int> Handle(DeleteItemCommand query)
        {
            var item = await _context.Items
                .FirstOrDefaultAsync(x => x.Id == query.Id);

            if (item == null)
            {
                throw new EntityNotFoundException();
            }
            try
            {
                _context.Items.Remove(item);
                await _context.SaveChangesAsync();

                _loggerService.LogDeleteItemEvent(query.Id, Status.Success, ApplicationType.CatalogApi);
            }
            catch (Exception ex)
            {
                _loggerService.LogDeleteItemEvent(query.Id, Status.Failure, ex.Message);
                throw;
            }

            try
            {
                var updateEvent = new Event<Item> { EventType = EventTypes.ProductDeleted, Data = item };
                await _serviceBusService.SendMessage(JsonConvert.SerializeObject(updateEvent));

                _loggerService.LogSendMessageEvent(query.Id, Status.Success, ApplicationType.CatalogApi);
            }
            catch (Exception ex)
            {
                _loggerService.LogSendMessageEvent(query.Id, Status.Failure, ex.Message);
                throw;
            }

            return item.Id;
        }
    }
}
