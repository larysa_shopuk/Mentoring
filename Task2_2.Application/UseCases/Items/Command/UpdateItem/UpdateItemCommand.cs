﻿using Task2_2.Application.Interfaces;
using Task2_2.Domain;

namespace Task2_2.Application.UseCases.Items.Command.UpdateItem
{
    public class UpdateItemCommand : ItemCommandBase, IRequest<Item>
    {
        public int Id { get; set; }
    }
}
