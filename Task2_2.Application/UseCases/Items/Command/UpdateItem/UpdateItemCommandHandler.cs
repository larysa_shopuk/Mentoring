﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Polly;
using Polly.CircuitBreaker;
using Task2_2.Application.Exceptions;
using Task2_2.Application.Interfaces;
using Task2_2.Core.Interfaces;
using Task2_2.Domain;
using Task4.Eventing.Contract;
using Task4.Eventing.Contract.Constants;
using Task4.Models;
using Task4.ServiceBusClient.Contracts;
using Task7.Logging;
using Task7.Logging.Constants;
using Task7.Logging.Extensions;

namespace Task2_2.Application.UseCases.Items.Command.UpdateItem
{
    public class UpdateItemCommandHandler : IRequestHandler<UpdateItemCommand, Item>
    {
        public const string ActivityName = "MessageProcessing";
        protected readonly IContext _context;
        private readonly IMapper _mapper;
        private readonly IServiceBusSender _serviceBusService;
        private readonly AsyncCircuitBreakerPolicy _circuitBreakerPolicy;
        private readonly IOptions<CircuitBreakerOptions> _circuitBreakerOptions;
        private readonly ILogger<UpdateItemCommandHandler> _logger;
        private readonly ILoggerService _loggerService;

        public UpdateItemCommandHandler(IContext context, IMapper mapper, IServiceBusSender serviceBusService,
            IOptions<CircuitBreakerOptions> circuitBreakerOptions, ILogger<UpdateItemCommandHandler> logger,
            TelemetryConfiguration telemetryConfiguration, ILoggerService loggerService)
        {
            _context = context;
            _mapper = mapper;
            _serviceBusService = serviceBusService;
            _circuitBreakerOptions = circuitBreakerOptions;
            _logger = logger;
            _loggerService = loggerService;
            _circuitBreakerPolicy = Policy.Handle<Exception>()
                .CircuitBreakerAsync(1, TimeSpan.FromMinutes(1),
                    (ex, t) => { _logger.LogInformation("Circuit broken!"); },
                    () => { _logger.LogInformation("Circuit Reset!"); });
        }

        public async Task<Item> Handle(UpdateItemCommand query)
        {

            var item = await _context.Items.AsTracking()
                .FirstOrDefaultAsync(x => x.Id == query.Id);

            if (item == null)
            {
                throw new EntityNotFoundException();
            }

            var entity = _mapper.Map<Item>(query);
            try
            {
                item.Updated = DateTime.Now;
                item.Id = query.Id;

                await _context.SaveChangesAsync();
                _loggerService.LogUpdateItemEvent(query.Id, Status.Success, ApplicationType.CatalogApi);
            }
            catch (Exception ex)
            {
                _loggerService.LogUpdateItemEvent(query.Id, Status.Failure, ex.Message);
                throw;
            }

            try
            {
                var updateEvent = new Event<Item>
                    { EventType = EventTypes.ProductUpdated, Data = entity };
                _logger.LogInformation($"Circuit State: {_circuitBreakerPolicy.CircuitState}");
                await _circuitBreakerPolicy.ExecuteAsync(async () =>
                {
                    await _serviceBusService.SendMessage(JsonConvert.SerializeObject(updateEvent));
                });

                _loggerService.LogSendMessageEvent(query.Id, Status.Success, ApplicationType.CatalogApi);

            }
            catch (Exception ex)
            {
                _loggerService.LogSendMessageEvent(query.Id, Status.Failure, ex.Message);
                throw;
            }

            return entity;
        }
    }
}
