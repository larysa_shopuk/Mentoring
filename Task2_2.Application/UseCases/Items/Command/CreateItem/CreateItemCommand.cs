﻿using Task2_2.Application.Interfaces;

namespace Task2_2.Application.UseCases.Items.Command.CreateItem
{
    public class CreateItemCommand : ItemCommandBase, IRequest<int>
    {
    }
}
