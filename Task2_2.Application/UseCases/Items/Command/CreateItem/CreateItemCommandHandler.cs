﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;
using Task2_2.Application.Exceptions;
using Task2_2.Application.Interfaces;
using Task2_2.Application.UseCases.Categories.Command.CreateCategory;
using Task2_2.Core.Interfaces;
using Task2_2.Domain;
using Task7.Logging;
using Task7.Logging.Constants;
using Task7.Logging.Extensions;

namespace Task2_2.Application.UseCases.Items.Command.CreateItem
{
    public class CreateItemCommandHandler : IRequestHandler<CreateItemCommand, int>
    {
        protected readonly IContext _context;
        private readonly IMapper _mapper;
        private readonly ILoggerService _loggerService;

        public CreateItemCommandHandler(IContext context, IMapper mapper, ILoggerService loggerService)
        {
            _context = context;
            _mapper = mapper;
            _loggerService = loggerService;
        }

        public async Task<int> Handle(CreateItemCommand query)
        {
            var item = _mapper.Map<Item>(query);
            try
            {
                await _context.Items.AddAsync(item);
                await _context.SaveChangesAsync();

                _loggerService.LogAddItemEvent(item.Id, Status.Success, ApplicationType.CatalogApi);
            }
            catch (SqlException se)
            {
                _loggerService.LogAddItemEvent(null, Status.Failure,
                    $"Failed to add the following item {JsonConvert.SerializeObject(query)}. Reason: {se.Message}");
                throw new InvalidModelException($"Parent category with '{query.ParentId}' does not exist");
            }
            catch (Exception ex)
            {
                _loggerService.LogAddItemEvent(null, Status.Failure, $"Failed to add the following item {JsonConvert.SerializeObject(query)}. Reason: {ex.Message}");
                throw;
            }

            return item.Id;
        }
    }
}
