﻿namespace Task2_2.Application.UseCases.Items
{
    public class ItemCommandBase
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }

        public int Amount { get; set; }

        public decimal Price { get; set; }

        public int ParentId { get; set; }
    }
}
