﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Task2_2.Application.Interfaces;
using Task2_2.Domain;

namespace Task2_2.Application.UseCases.Items.Query.GetItem
{
    public class GetItemQueryHandler : GetQueryHandler<GetItemQuery, Item>
    {
        public GetItemQueryHandler(IContext context) : base(context)
        {
        }

        protected override async Task<Item> GetEntityQuery(GetItemQuery request)
        {
            return await Context.Items.Include(x => x.Parent).FirstOrDefaultAsync(x => x.Id == request.Id);
        }
    }
}
