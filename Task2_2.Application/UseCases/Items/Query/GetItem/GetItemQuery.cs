﻿using Task2_2.Application.Interfaces;
using Task2_2.Domain;

namespace Task2_2.Application.UseCases.Items.Query.GetItem
{
    public class GetItemQuery : IRequest<Item>
    {
        public int Id { get; set; }
    }
}
