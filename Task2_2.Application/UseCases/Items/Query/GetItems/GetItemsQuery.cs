﻿using Task2_2.Application.Interfaces;
using Task2_2.Application.Models;
using Task2_2.Domain;

namespace Task2_2.Application.UseCases.Items.Query.GetItems
{
    public class GetItemsQuery : IRequest<PaginatedResult<Item>>
    {
        public int StartPage { get; set; }

        public int PageSize { get; set; }
    }
}
