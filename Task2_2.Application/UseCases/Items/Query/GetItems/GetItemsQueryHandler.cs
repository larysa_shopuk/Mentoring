﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Task2_2.Application.Interfaces;
using Task2_2.Application.Models;
using Task2_2.Core.Interfaces;
using Task2_2.Domain;

namespace Task2_2.Application.UseCases.Items.Query.GetItems
{
    public class GetItemsQueryHandler : IRequestHandler<GetItemsQuery, PaginatedResult<Item>>
    {
        protected readonly IContext _context;

        public GetItemsQueryHandler(IContext context)
        {
            _context = context;
        }

        public Task<PaginatedResult<Item>> Handle(GetItemsQuery query)
        {
            var page = query.StartPage < 1 ? 1 : query.StartPage;
            var items = _context.Items.AsQueryable();
            var itemsNumber = items.Count();
            var itemsPage = items.Skip(query.PageSize * (page - 1)).Take(query.PageSize).Include(i => i.Parent).ToList();

            return Task.FromResult(new PaginatedResult<Item>
                {Items = itemsPage, Page = query.StartPage, Total = itemsNumber, PageSize = query.PageSize});
        }
    }
}
