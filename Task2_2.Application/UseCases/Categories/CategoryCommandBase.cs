﻿namespace Task2_2.Application.UseCases.Categories
{
    public class CategoryCommandBase
    {
        public string Name { get; set; }

        public string Image { get; set; }

        public int? ParentId { get; set; }
    }
}
