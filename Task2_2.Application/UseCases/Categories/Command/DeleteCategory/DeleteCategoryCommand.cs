﻿using Task2_2.Application.Interfaces;

namespace Task2_2.Application.UseCases.Categories.Command.DeleteCategory
{
    public class DeleteCategoryCommand : IRequest<int>
    {
        public int Id { get; set; }
    }
}
