﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Task2_2.Application.Exceptions;
using Task2_2.Application.Interfaces;
using Task2_2.Core.Interfaces;
using Task7.Logging;
using Task7.Logging.Constants;
using Task7.Logging.Extensions;

namespace Task2_2.Application.UseCases.Categories.Command.DeleteCategory
{
    public class DeleteCategoryCommandHandler : IRequestHandler<DeleteCategoryCommand, int>
    {
        protected readonly IContext _context;
        private readonly ILoggerService _loggerService;

        public DeleteCategoryCommandHandler(IContext context, ILoggerService loggerService)
        {
            _context = context;
            _loggerService = loggerService;
        }

        public async Task<int> Handle(DeleteCategoryCommand query)
        {
            var category = await _context.Categories
                .FirstOrDefaultAsync(x => x.Id == query.Id);

            if (category == null)
            {
                throw new EntityNotFoundException();
            }

            try
            {
                _context.Categories.Remove(category);
                await _context.SaveChangesAsync();
                _loggerService.LogDeleteItemEvent(query.Id, Status.Success, ApplicationType.CatalogApi);
            }
            catch (Exception ex)
            {
                _loggerService.LogDeleteItemEvent(query.Id, Status.Failure, ex.Message);
                throw;
            }

            return category.Id;
        }
    }
}
