﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Task2_2.Application.Exceptions;
using Task2_2.Application.Interfaces;
using Task2_2.Core.Interfaces;
using Task2_2.Domain;
using Task7.Logging;
using Task7.Logging.Constants;
using Task7.Logging.Extensions;

namespace Task2_2.Application.UseCases.Categories.Command.UpdateCategory
{
    public class UpdateCategoryCommandHandler : IRequestHandler<UpdateCategoryCommand, Category>
    {
        protected readonly IContext _context;
        private readonly IMapper _mapper;
        private readonly ILoggerService _loggerService;

        public UpdateCategoryCommandHandler(IContext context, IMapper mapper, ILoggerService loggerService)
        {
            _context = context;
            _mapper = mapper;
            _loggerService = loggerService;
        }

        public async Task<Category> Handle(UpdateCategoryCommand query)
        {
            var category = await _context.Categories
                .FirstOrDefaultAsync(x => x.Id == query.Id);

            if (category == null)
            {
                throw new EntityNotFoundException();
            }

            try
            {
                var entity = _mapper.Map<Category>(query);
                _mapper.Map(entity, category);
                category.Updated = DateTime.Now;

                await _context.SaveChangesAsync();
                _loggerService.LogUpdateItemEvent(query.Id, Status.Success, ApplicationType.CatalogApi);

                return entity;
            }
            catch (Exception ex)
            {
                _loggerService.LogUpdateItemEvent(query.Id, Status.Failure, ex.Message);
                throw;
            }
        }
    }
}
