﻿using Task2_2.Application.Interfaces;
using Task2_2.Domain;

namespace Task2_2.Application.UseCases.Categories.Command.UpdateCategory
{
    public class UpdateCategoryCommand : CategoryCommandBase, IRequest<Category>
    {
        public int Id { get; set; }
    }
}
