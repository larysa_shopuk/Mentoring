﻿using Task2_2.Application.Interfaces;

namespace Task2_2.Application.UseCases.Categories.Command.CreateCategory
{
    public class CreateCategoryCommand : CategoryCommandBase, IRequest<int>
    {
    }
}
