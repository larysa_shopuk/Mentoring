﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;
using Task2_2.Application.Exceptions;
using Task2_2.Application.Interfaces;
using Task2_2.Core.Interfaces;
using Task2_2.Domain;
using Task7.Logging;
using Task7.Logging.Constants;
using Task7.Logging.Extensions;

namespace Task2_2.Application.UseCases.Categories.Command.CreateCategory
{
    public class CreateCategoryCommandHandler : IRequestHandler<CreateCategoryCommand, int>
    {
        protected readonly IContext _context;
        private readonly IMapper _mapper;
        private readonly ILoggerService _loggerService;

        public CreateCategoryCommandHandler(IContext context, IMapper mapper, ILoggerService loggerService)
        {
            _context = context;
            _mapper = mapper;
            _loggerService = loggerService;
        }

        public async Task<int> Handle(CreateCategoryCommand query)
        {
            var category = _mapper.Map<Category>(query);
            try
            {
                await _context.Categories.AddAsync(category);
                await _context.SaveChangesAsync();
                _loggerService.LogAddItemEvent(category.Id, Status.Success, ApplicationType.CatalogApi);
            }
            catch (SqlException se)
            {
                _loggerService.LogAddItemEvent(null, Status.Failure, $"Failed to add the following item {JsonConvert.SerializeObject(query)}. Reason: {se.Message}");
                throw new InvalidModelException($"Parent category with '{query.ParentId}' does not exist");
            }
            catch (Exception ex)
            {
                _loggerService.LogAddItemEvent(null, Status.Failure, $"Failed to add the following item {JsonConvert.SerializeObject(query)}. Reason: {ex.Message}");
                throw;
            }

            return category.Id;
        }
    }
}
