﻿using Task2_2.Application.Interfaces;
using Task2_2.Domain;

namespace Task2_2.Application.UseCases.Categories.Query.GetCategory
{
    public class GetCategoryQuery : IRequest<Category>
    {
        public int Id { get; set; }
    }
}
