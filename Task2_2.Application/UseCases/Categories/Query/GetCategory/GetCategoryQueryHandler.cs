﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Task2_2.Application.Interfaces;
using Task2_2.Domain;

namespace Task2_2.Application.UseCases.Categories.Query.GetCategory
{
    public class GetCategoryQueryHandler : GetQueryHandler<GetCategoryQuery, Category>
    {
        public GetCategoryQueryHandler(IContext context) : base(context)
        {
        }

        protected override async Task<Category> GetEntityQuery(GetCategoryQuery request)
        {
            return await Context.Categories.Include(x => x.Parent).FirstOrDefaultAsync(x => x.Id == request.Id);
        }
    }
}
