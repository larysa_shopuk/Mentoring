﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Task2_2.Application.Interfaces;
using Task2_2.Application.Models;
using Task2_2.Core.Interfaces;
using Task2_2.Domain;

namespace Task2_2.Application.UseCases.Categories.Query.GetCategories
{
    public class GetCategoriesQueryHandler : IRequestHandler<GetCategoriesQuery, PaginatedResult<Category>>
    {
        protected readonly IContext _context;

        public GetCategoriesQueryHandler(IContext context)
        {
            _context = context;
        }

        public Task<PaginatedResult<Category>> Handle(GetCategoriesQuery query)
        {
            var page = query.StartPage < 1 ? 1 : query.StartPage;
            var categories = _context.Categories.AsQueryable();
            var itemsNumber = categories.Count();
            var categoriesPage = categories.Skip(query.PageSize * (page - 1)).Take(query.PageSize).Include(x => x.Parent).ToList();

            return Task.FromResult(new PaginatedResult<Category>
                {Items = categoriesPage, Page = query.StartPage, Total = itemsNumber, PageSize = query.PageSize});
        }
    }
}
