﻿using Task2_2.Application.Interfaces;
using Task2_2.Application.Models;
using Task2_2.Domain;

namespace Task2_2.Application.UseCases.Categories.Query.GetCategories
{
    public class GetCategoriesQuery : IRequest<PaginatedResult<Category>>
    {
        public int StartPage { get; set; }

        public int PageSize { get; set; }
    }
}
