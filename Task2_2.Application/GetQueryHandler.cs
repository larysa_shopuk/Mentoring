﻿using System.Threading.Tasks;
using Task2_2.Application.Exceptions;
using Task2_2.Application.Interfaces;
using Task2_2.Core.Interfaces;

namespace Task2_2.Application
{
    public abstract class GetQueryHandler<TQuery, TResponse> : IRequestHandler<TQuery, TResponse>
        where TQuery : IRequest<TResponse>
    {
        protected readonly IContext Context;

        protected GetQueryHandler(IContext context)
        {
            Context = context;
        }

        public async Task<TResponse> Handle(TQuery request)
        {
            var entity = await GetEntityQuery(request);

            if (entity == null)
            {
                throw new EntityNotFoundException();
            }

            return entity;
        }

        protected abstract Task<TResponse> GetEntityQuery(TQuery request);
    }
}
