﻿using System.Threading;
using System.Threading.Tasks;
using Task2_2.Domain;
using Microsoft.EntityFrameworkCore;

namespace Task2_2.Application.Interfaces
{
    public interface IContext
    {
        DbSet<Category> Categories { get; set; }

        DbSet<Item> Items { get; set; }

        int SaveChanges();

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}
