﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Task3_1.Api.Contract;
using Task5.CatalogApiClient.Models;
using Task5.CatalogApiClient.Services;

namespace Task5.CatalogApiClient.Controllers
{
    [Authorize]
    public class CallApiController : Controller
    {
        private readonly ITokenService _tokenService;
        private readonly ApiClientSettings _apiClientSettings;

        public CallApiController(ITokenService tokenService, IOptions<ApiClientSettings> apiClientSettings)
        {
            _tokenService = tokenService;
            _apiClientSettings = apiClientSettings.Value;
        }

        [Authorize(Roles = "Manager, Buyer")]
        public async Task<IActionResult> GetItems()
        {
            using var client = new HttpClient();
            var tokenResponse = await _tokenService.GetToken(_apiClientSettings.Scope);

            client
                .SetBearerToken(tokenResponse.AccessToken);

            var result = client
                .GetAsync($"{_apiClientSettings.BaseUrl}v1/items?startPage=1&pageSize=100")
                .Result;

            if (result.IsSuccessStatusCode)
            {
                var model = result.Content.ReadAsStringAsync().Result;
                var itemsPage = JsonConvert.DeserializeObject<Page<Item>>(model);

                return View("Categories", itemsPage.Items);
            }

            throw new Exception("Unable to get content");
        }
        
        public IActionResult AddItemToCategory(int categoryId)
        {
            if (!User.IsInRole("Manager"))
            {
                return Unauthorized();
            }

            return View("AddItem", categoryId);

        }

        [HttpPost]
        [Authorize(Roles = "Manager")]
        public async Task<IActionResult> AddItemToCategory(Item item)
        {
            using var client = new HttpClient();
            var tokenResponse = await _tokenService.GetToken(_apiClientSettings.Scope);

            client
                .SetBearerToken(tokenResponse.AccessToken);
            HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(item), Encoding.UTF8);
            httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var result = client
                .PostAsync($"{_apiClientSettings.BaseUrl}v1/items", httpContent)
                .Result;

            if (result.IsSuccessStatusCode)
            {
                return RedirectToAction("GetItems", "CallApi");
            }

            throw new Exception("Unable to add the item");
        }

        public IActionResult Claims()
        {
            return View();
        }
    }
}
