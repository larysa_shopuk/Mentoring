using System.Threading.Tasks;
using IdentityModel.Client;

namespace Task5.CatalogApiClient.Services
{
  public interface ITokenService
  {
    Task<TokenResponse> GetToken(string scope);
  }
}