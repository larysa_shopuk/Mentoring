﻿namespace Task5.CatalogApiClient.Models
{
    public class ApiClientSettings
    {
        public string BaseUrl { get; set; }

        public string Scope { get; set; }
    }
}
