﻿using System.Collections.Generic;

namespace Task5.CatalogApiClient.Models
{
    public class Page<T>
    {
        public List<T> Items { get; set; }
    }
}
