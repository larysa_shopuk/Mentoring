﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Task2_2.Domain;

namespace Task2_2.Infrastructure.Configuration
{
    public class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.Property(e => e.Id)
                .ValueGeneratedOnAdd();

            builder.Property(e => e.Name)
                .HasMaxLength(50);

            builder.Property(e => e.Image)
                .IsRequired(false)
                .HasMaxLength(255);

            builder.HasOne(p => p.Parent)
                .WithMany()
                .HasForeignKey(p => p.ParentId)
                .HasPrincipalKey(s => s.Id)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.NoAction);

            builder.Property(e => e.Created)
                .HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");

            builder.Property(e => e.Updated)
                .HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");
        }
    }
}
