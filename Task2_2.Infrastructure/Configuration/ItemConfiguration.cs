﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Task2_2.Domain;

namespace Task2_2.Infrastructure.Configuration
{
    public class ItemConfiguration : IEntityTypeConfiguration<Item>
    {
        public void Configure(EntityTypeBuilder<Item> builder)
        {
            builder.Property(e => e.Id)
                .ValueGeneratedOnAdd();

            builder.Property(e => e.Name)
                .HasMaxLength(50);

            builder.Property(e => e.Description)
                .IsRequired(false)
                .HasMaxLength(255);

            builder.Property(e => e.Image)
                .IsRequired(false)
                .HasMaxLength(255);

            builder.Property(e => e.Price)
                .HasColumnType("money");

            builder.Property(e => e.Amount)
                .HasColumnType("int");

            builder.HasOne(p => p.Parent)
                .WithMany(p => p.Items)
                .HasForeignKey(p => p.ParentId)
                .HasConstraintName("FK_Items_ParentId")
                .OnDelete(DeleteBehavior.Cascade);

            builder.Property(e => e.Created)
                .HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");

            builder.Property(e => e.Updated)
                .HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");
        }
    }
}
