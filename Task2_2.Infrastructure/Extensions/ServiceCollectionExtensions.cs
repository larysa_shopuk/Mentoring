﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Task2_2.Application.Interfaces;

namespace Task2_2.Infrastructure.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDatabase(this IServiceCollection services)
        {
            var serviceProvider = services.BuildServiceProvider();
            var configuration = serviceProvider.GetService<IConfiguration>();
            services
                .AddDbContext<CatalogContext>(options => ConfigureSqlServer(options, configuration))
                .AddScoped<IContext>(provider => provider.GetService<CatalogContext>());

            return services;
        }
        
        public static void ConfigureSqlServer(DbContextOptionsBuilder options, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("DbContext");
            options.UseSqlServer(connectionString);
            options.EnableSensitiveDataLogging();
            options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
        }
    }
}
