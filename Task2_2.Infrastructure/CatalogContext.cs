﻿using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Task2_2.Application.Interfaces;
using Task2_2.Domain;

namespace Task2_2.Infrastructure
{
    public class CatalogContext : DbContext, IContext
    {
        public CatalogContext()
        {
        }

        public CatalogContext(DbContextOptions<CatalogContext> options)
            : base(options)
        {
        }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Item> Items { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var assembly = Assembly.GetExecutingAssembly();
            modelBuilder.ApplyConfigurationsFromAssembly(assembly);
        }
    }
}
