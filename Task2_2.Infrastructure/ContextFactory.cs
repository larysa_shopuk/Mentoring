﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Task2_2.Infrastructure
{
    public class ContextFactory : IDesignTimeDbContextFactory<CatalogContext>
    {
        public CatalogContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<CatalogContext>();
            var connectionString = "Server=.;Trusted_Connection=true;Database=Products";

            builder.UseSqlServer(connectionString);
            return new CatalogContext(builder.Options);
        }
    }
}
