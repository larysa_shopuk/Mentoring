﻿using System.Collections.Generic;

namespace Task2_2.Domain
{
    public class Category : Entity
    {
        public Category()
        {
            Items = new HashSet<Item>();
        }

        public string Name { get; set; }

        public string Image { get; set; }

        public int? ParentId { get; set; }

        public virtual Category Parent { get; set; }

        public virtual ICollection<Item> Items { get; set; }
    }
}
