﻿using System;

namespace Task2_2.Domain
{
    public class Entity
    {
        public Entity()
        {
            Created = Updated = DateTime.Now;
        }

        public int Id { get; set; }

        public DateTime Created { get; set; }

        public DateTime Updated { get; set; }
    }
}
