﻿using System.Collections.Generic;

namespace Task2_2.Domain
{
    public class Item : Entity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }

        public int Amount { get; set; }

        public decimal Price { get; set; }

        public int ParentId { get; set; }

        public virtual Category Parent { get; set; }
    }
}
