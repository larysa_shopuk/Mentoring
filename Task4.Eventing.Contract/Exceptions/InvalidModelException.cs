﻿using System;

namespace Task4.Eventing.Contract.Exceptions
{
    public class InvalidModelException : Exception
    {
        public InvalidModelException(string message) : base(message)
        {
        }
    }
}