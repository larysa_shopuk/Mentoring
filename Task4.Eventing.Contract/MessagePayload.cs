﻿namespace Task4.Eventing.Contract
{
    public class MessagePayload
    {
        public string RootId { get; set; }

        public string ParentId { get; set; }

        public string Payload { get; set; }
    }
}
