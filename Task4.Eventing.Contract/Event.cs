﻿namespace Task4.Eventing.Contract
{
    public class Event<T>
    {
        public string EventType { get; set; }

        public T Data { get; set; }

        public string CorrelationId { get; set; }
    }
}
