﻿namespace Task4.Eventing.Contract.Constants
{
    public class EventTypes
    {
        public const string ProductUpdated = "ProductUpdated";

        public const string ProductDeleted = "ProductDeleted";
    }
}
