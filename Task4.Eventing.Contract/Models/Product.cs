﻿namespace Task4.Eventing.Contract.Models
{
    public class Product
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }

        public int Amount { get; set; }

        public decimal Price { get; set; }

        public int ParentId { get; set; }
    }
}
