﻿using System;
using System.Collections.Generic;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.ApplicationInsights.Extensibility;

namespace Task7.Logging
{
    public class ApplicationInsightLoggerService : ILoggerService
    {
        private readonly TelemetryClient _client;

        public ApplicationInsightLoggerService(TelemetryConfiguration telemetryConfiguration)
        {
            _client = new TelemetryClient(telemetryConfiguration);
        }

        public void LogEvent(string eventName, Dictionary<string, string> properties = null)
        {
            if (string.IsNullOrEmpty(eventName))
            {
                throw new ArgumentNullException(nameof(eventName));
            }

            var telemetry = new EventTelemetry(eventName);
            if (properties != null)
            {
                foreach (var (prop, value) in properties)
                {
                    telemetry.Properties.TryAdd(prop, value);
                }
            }

            _client.TrackEvent(telemetry);
        }
    }
}
