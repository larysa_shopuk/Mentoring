﻿namespace Task7.Logging.Constants
{
    public static class LogConstants
    {
        public const string ItemId = nameof(ItemId);

        public const string Status = nameof(Status);

        public const string Application = nameof(Application);

        public const string Message = nameof(Message);

        public const string EventContent = nameof(EventContent);
    }
}
