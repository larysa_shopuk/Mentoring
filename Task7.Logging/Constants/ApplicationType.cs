﻿namespace Task7.Logging.Constants
{
    public class ApplicationType
    {
        public const string CatalogApi = nameof(CatalogApi);

        public const string CartApi = nameof(CartApi);
    }
}
