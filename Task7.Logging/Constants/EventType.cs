﻿namespace Task7.Logging.Constants
{
    public class EventType
    {
        public const string DeleteItemEvent = "DeleteItem";

        public const string UpdateItemEvent = "UpdateItem";

        public const string AddItemEvent = "AddItem";

        public const string SendMessageEvent = "SendMessage";

        public const string MessageReceivedEvent = "MessageReceived";

        public const string MessageValidationFailedEvent = "MessageValidationFailed";

        public const string MessageProcessingFailedEvent = "MessageProcessingFailed";

        public const string MessageProcessingFinishedEvent = "MessageProcessingFinishedEvent";
    }
}
