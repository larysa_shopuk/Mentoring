﻿using System.Collections.Generic;

namespace Task7.Logging
{
    public interface ILoggerService
    {
        void LogEvent(string eventName, Dictionary<string, string> properties = null);
    }
}
