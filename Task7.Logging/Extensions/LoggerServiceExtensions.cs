﻿using System.Collections.Generic;
using Task7.Logging.Constants;

namespace Task7.Logging.Extensions
{
    public static class LoggerServiceExtensions
    {
        public static void LogDeleteItemEvent(this ILoggerService loggerService, int? itemId, Status status, string applicationType, string message = null)
        {
            loggerService.LogItemEvent(EventType.DeleteItemEvent, itemId, status, applicationType, message);
        }

        public static void LogUpdateItemEvent(this ILoggerService loggerService, int? itemId, Status status, string applicationType, string message = null)
        {
            loggerService.LogItemEvent(EventType.UpdateItemEvent, itemId, status, applicationType, message);
        }

        public static void LogAddItemEvent(this ILoggerService loggerService, int? itemId, Status status, string applicationType, string message = null)
        {
            loggerService.LogItemEvent(EventType.AddItemEvent, itemId, status, applicationType, message);
        }

        public static void LogSendMessageEvent(this ILoggerService loggerService, int? itemId, Status status, string applicationType, string message = null)
        {
            loggerService.LogItemEvent(EventType.SendMessageEvent, itemId, status, applicationType, message);
        }

        public static void LogProcessMessageEvent(this ILoggerService loggerService, string eventName, string content, string message = null)
        {
            var properties = new Dictionary<string, string>
            {
                [LogConstants.EventContent] = content,
                [LogConstants.Message] = message
            };

            loggerService.LogEvent(eventName, properties);
        }

        public static void LogItemEvent(this ILoggerService loggerService, string eventName, int? itemId, Status status, string applicationType, string message = null)
        {
            var properties = new Dictionary<string, string>
            {
                [LogConstants.ItemId] = itemId?.ToString(),
                [LogConstants.Status] = status.ToString(),
                [LogConstants.Application] = applicationType,
                [LogConstants.Message] = message
            };

            loggerService.LogEvent(eventName, properties);
        }
    }
}
