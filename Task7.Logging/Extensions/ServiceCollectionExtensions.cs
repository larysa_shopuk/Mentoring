﻿using Microsoft.Extensions.DependencyInjection;

namespace Task7.Logging.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddTelemetryLogging(this IServiceCollection services)
        {
            services.AddLogging();

            return services
                .AddApplicationInsightsTelemetry()
                .AddSingleton<ILoggerService, ApplicationInsightLoggerService>();
        }
    }
}
