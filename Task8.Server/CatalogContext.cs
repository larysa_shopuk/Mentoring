﻿using Microsoft.EntityFrameworkCore;
using Task2_2.Domain;

namespace Task8.Server
{
    public class CatalogContext : DbContext
    {
        public CatalogContext(DbContextOptions<CatalogContext> options)
            : base(options)
        {
        }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Item> Items { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().Property(e => e.Id)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<Category>().Property(e => e.Name)
                .HasMaxLength(50);

            modelBuilder.Entity<Category>().Property(e => e.Image)
                .IsRequired(false)
                .HasMaxLength(255);

            modelBuilder.Entity<Category>().HasOne(p => p.Parent)
                .WithMany()
                .HasForeignKey(p => p.ParentId)
                .HasPrincipalKey(s => s.Id)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Category>().Property(e => e.Created)
                .HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");

            modelBuilder.Entity<Category>().Property(e => e.Updated)
                .HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");

            modelBuilder.Entity<Category>().HasOne(p => p.Parent)
                .WithMany()
                .HasForeignKey(p => p.ParentId)
                .HasPrincipalKey(s => s.Id)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.NoAction);


            modelBuilder
                .Entity<Item>().Property(e => e.Id)
                .ValueGeneratedOnAdd();

            modelBuilder
                .Entity<Item>().Property(e => e.Name)
                .HasMaxLength(50);

            modelBuilder
                .Entity<Item>().Property(e => e.Description)
                .IsRequired(false)
                .HasMaxLength(255);

            modelBuilder
                .Entity<Item>().Property(e => e.Image)
                .IsRequired(false)
                .HasMaxLength(255);

            modelBuilder
                .Entity<Item>().Property(e => e.Price)
                .HasColumnType("money");

            modelBuilder
                .Entity<Item>().Property(e => e.Amount)
                .HasColumnType("int");

            modelBuilder
                .Entity<Item>().HasOne(p => p.Parent)
                .WithMany(p => p.Items)
                .HasForeignKey(p => p.ParentId)
                .HasConstraintName("FK_Items_ParentId")
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder
                .Entity<Item>().Property(e => e.Created)
                .HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");

            modelBuilder
                .Entity<Item>().Property(e => e.Updated)
                .HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");
        }
    }
}
