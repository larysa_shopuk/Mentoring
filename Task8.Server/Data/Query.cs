﻿using System.Linq;
using HotChocolate;
using HotChocolate.Data;

namespace Task8.Server.Data
{
    [GraphQLDescription("Represents the queries available.")]
    public class Query
    {
        [UseDbContext(typeof(CatalogContext))]
        [UseProjection]
        [UseFiltering]
        [UseSorting]
        [GraphQLDescription("Gets the queryable category.")]
        public IQueryable<Task2_2.Domain.Category> GetCategories(PageInfoInput input, [ScopedService] CatalogContext context)
        {
            var toTake = input?.pageSize ?? 10;
            var skipNumber = input?.pageNumber ?? 1 * toTake;
            return context.Categories.Skip(skipNumber).Take(toTake);
        }

        [UseDbContext(typeof(CatalogContext))]
        [UseProjection]
        [UseFiltering]
        [UseSorting]
        [GraphQLDescription("Gets the queryable item.")]
        public IQueryable<Task2_2.Domain.Item> GetItems(PageInfoInput input, [ScopedService] CatalogContext context)
        {
            var toTake = input?.pageSize ?? 10;
            var skipNumber = input?.pageNumber ?? 1 * toTake;

            return context.Items.Skip(skipNumber).Take(toTake);
        }
    }
}
