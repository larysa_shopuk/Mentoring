﻿namespace Task8.Server.Data.Item.Delete
{
    public record DeleteItemInput(int itemId);
}
