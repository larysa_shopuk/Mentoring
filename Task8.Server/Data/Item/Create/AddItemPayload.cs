﻿namespace Task8.Server.Data.Item.Create
{
    public record AddItemPayload(Task2_2.Domain.Item data);
}
