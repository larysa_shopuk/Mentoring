﻿namespace Task8.Server.Data.Item.Create
{
    public record AddItemInput(string name, string description, string image, int parentId, decimal price, int amount);
}
