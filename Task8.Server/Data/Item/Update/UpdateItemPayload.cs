﻿namespace Task8.Server.Data.Item.Update
{
    public record UpdateItemPayload(Task2_2.Domain.Item data);
}
