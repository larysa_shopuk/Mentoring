﻿namespace Task8.Server.Data.Item.Update
{
    public record UpdateItemInput(int itemId, string name, string description, string image, int parentId, decimal price, int amount);
}
