﻿namespace Task8.Server.Data
{
    public record PageInfoInput(int? pageNumber = 1, int? pageSize = 10);
}
