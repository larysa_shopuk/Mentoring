﻿using System;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Data;
using Microsoft.EntityFrameworkCore;
using Task8.Server.Data.Category.Create;
using Task8.Server.Data.Category.Delete;
using Task8.Server.Data.Category.Update;
using Task8.Server.Data.Item.Create;
using Task8.Server.Data.Item.Delete;
using Task8.Server.Data.Item.Update;

namespace Task8.Server.Data
{
    public class Mutation
    {
        [UseDbContext(typeof(CatalogContext))]
        public async Task<AddCategoryPayload> AddCategoryAsync(AddCategoryInput input, [ScopedService] CatalogContext context)
        {
            var category = new Task2_2.Domain.Category
            {
                Name = input.name,
                Image = input.image,
                ParentId = input.parentId
            };

            context.Categories.Add(category);
            await context.SaveChangesAsync();

            return new AddCategoryPayload(category);
        }

        [UseDbContext(typeof(CatalogContext))]
        public async Task<UpdateCategoryPayload> UpdateCategoryAsync(UpdateCategoryInput input, [ScopedService] CatalogContext context)
        {
            var category = await context.Categories
                .FirstOrDefaultAsync(x => x.Id == input.categoryId);
            if (category == null)
            {
                throw new Exception("No category found");
            }

            category.Name = input.name;
            category.Image = input.image;
            category.ParentId = input.parentId;

            category.Updated = DateTime.Now;

            await context.SaveChangesAsync();

            return new UpdateCategoryPayload(category);
        }

        [UseDbContext(typeof(CatalogContext))]
        public async Task<bool> DeleteCategoryAsync(DeleteCategoryInput input, [ScopedService] CatalogContext context)
        {
            var category = await context.Categories
                .FirstOrDefaultAsync(x => x.Id == input.categoryId);
            
            if (category == null)
            {
                throw new Exception("No category found");
            }

            context.Categories.Remove(category);
            await context.SaveChangesAsync();

            return true;
        }

        [UseDbContext(typeof(CatalogContext))]
        public async Task<AddItemPayload> AddItemAsync(AddItemInput input, [ScopedService] CatalogContext context)
        {
            var item = new Task2_2.Domain.Item
            {
                Name = input.name,
                Image = input.image,
                ParentId = input.parentId,
                Amount = input.amount,
                Description = input.description,
                Price = input.price
            };

            context.Items.Add(item);
            await context.SaveChangesAsync();

            return new AddItemPayload(item);
        }

        [UseDbContext(typeof(CatalogContext))]
        public async Task<UpdateItemPayload> UpdateItemAsync(UpdateItemInput input, [ScopedService] CatalogContext context)
        {
            var item = await context.Items
                .FirstOrDefaultAsync(x => x.Id == input.itemId);
            if (item == null)
            {
                throw new Exception("No Item found");
            }

            item.Name = input.name;
            item.Image = input.image;
            item.ParentId = input.parentId;
            item.Description = input.description;
            item.Amount = input.amount;
            item.Price = input.price;

            item.Updated = DateTime.Now;

            await context.SaveChangesAsync();

            return new UpdateItemPayload(item);
        }

        [UseDbContext(typeof(CatalogContext))]
        public async Task<bool> DeleteItemAsync(DeleteItemInput input, [ScopedService] CatalogContext context)
        {
            var item = await context.Items
                .FirstOrDefaultAsync(x => x.Id == input.itemId);

            if (item == null)
            {
                throw new Exception("No Item found");
            }

            context.Items.Remove(item);
            await context.SaveChangesAsync();

            return true;
        }
    }
}
