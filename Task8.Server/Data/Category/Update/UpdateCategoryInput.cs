﻿namespace Task8.Server.Data.Category.Update
{
    public record UpdateCategoryInput(int categoryId, string name, string image, int? parentId);
}
