﻿namespace Task8.Server.Data.Category.Update
{
    public record UpdateCategoryPayload(Task2_2.Domain.Category data);
}
