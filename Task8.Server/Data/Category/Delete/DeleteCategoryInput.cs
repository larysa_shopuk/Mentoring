﻿namespace Task8.Server.Data.Category.Delete
{
    public record DeleteCategoryInput(int categoryId);
}
