﻿namespace Task8.Server.Data.Category.Create
{
    public record AddCategoryInput(string name, string image, int? parentId);
}
