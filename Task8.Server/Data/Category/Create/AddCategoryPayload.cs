﻿namespace Task8.Server.Data.Category.Create
{
    public record AddCategoryPayload(Task2_2.Domain.Category data);
}
