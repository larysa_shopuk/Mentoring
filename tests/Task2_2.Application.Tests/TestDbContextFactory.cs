﻿using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Task2_2.Infrastructure;

namespace Task2_2.Application.Tests
{
    [ExcludeFromCodeCoverage]
    public static class TestDbContextFactory
    {
        public static CatalogContext CreateInMemory(string databaseName = null)
        {
            var builder = new DbContextOptionsBuilder<CatalogContext>(new DbContextOptions<CatalogContext>());
            builder.UseInMemoryDatabase(databaseName ?? Guid.NewGuid().ToString());
            return new CatalogContext(builder.Options);
        }
    }
}
