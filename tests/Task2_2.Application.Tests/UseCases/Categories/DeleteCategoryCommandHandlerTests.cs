﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Task2_2.Application.Exceptions;
using Task2_2.Application.Interfaces;
using Task2_2.Application.UseCases.Categories.Command.CreateCategory;
using Task2_2.Application.UseCases.Categories.Command.DeleteCategory;
using Task2_2.Application.UseCases.Categories.Query.GetCategory;
using Task2_2.Core.Interfaces;
using Task2_2.Domain;
using Xunit;

namespace Task2_2.Application.Tests.UseCases.Categories
{
    public class DeleteCategoryCommandHandlerTests
    {
        private readonly IServiceCollection _serviceCollection;

        public DeleteCategoryCommandHandlerTests()
        {
            _serviceCollection = new ServiceCollection();
            _serviceCollection.AddCommandHandler<DeleteCategoryCommand, DeleteCategoryCommandHandler, int>(MockDbContext);
        }

        [Fact]
        public async Task GivenDeleteCategoryCommand_WhenIsExecuted_ThenEntityShouldBeDeletedInDatabase()
        {
            await using var provider = _serviceCollection.BuildServiceProvider();
            var deleteCommand = new DeleteCategoryCommand
            {
                Id = 1
            };

            var initialEntitiesNumber = provider.GetService<IContext>().Categories.Count();

            var mediator = provider.GetService<IMediator>();
            await mediator.Send<DeleteCategoryCommand, int>(deleteCommand);
            
            var actualEntitiesNumber = provider.GetService<IContext>().Categories.Count();
            var entity = provider.GetService<IContext>().Categories.FirstOrDefault(x => x.Id == deleteCommand.Id);

            Assert.Null(entity);
            Assert.Equal(initialEntitiesNumber, actualEntitiesNumber + 1);
        }

        [Fact]
        public async Task GivenDeleteCategoryCommand_WhenEntityDoesNotExist_ThenExceptionShouldBeThrown()
        {
            await using var provider = _serviceCollection.BuildServiceProvider();
            var deleteCommand = new DeleteCategoryCommand
            {
                Id = 100500
            };

            var initialEntitiesNumber = provider.GetService<IContext>().Categories.Count();

            var mediator = provider.GetService<IMediator>();

            var actualEntitiesNumber = provider.GetService<IContext>().Categories.Count();
            var exception = await Assert.ThrowsAsync<EntityNotFoundException>(() => mediator.Send<DeleteCategoryCommand, int>(deleteCommand));

            Assert.Equal("Entity not found", exception.Message);
            Assert.Equal(initialEntitiesNumber, actualEntitiesNumber);
        }

        private void MockDbContext(IContext context)
        {
            var category = new Category
            {
                Name = "Test category"
            };

            context.Categories.Add(category);

            context.SaveChanges();
        }
    }
}
