using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Task2_2.Application.Exceptions;
using Task2_2.Application.Interfaces;
using Task2_2.Application.UseCases.Categories.Query.GetCategory;
using Task2_2.Core.Interfaces;
using Task2_2.Domain;
using Xunit;

namespace Task2_2.Application.Tests.UseCases.Categories
{
    public class GetCategoryQueryHandlerTests
    {
        private const int CategoryId = 1;

        private readonly IServiceCollection _serviceCollection;

        public GetCategoryQueryHandlerTests()
        {
            _serviceCollection = new ServiceCollection();
            _serviceCollection.AddHandler<GetCategoryQuery, Category, GetCategoryQueryHandler>(MockDbContext);
        }

        [Fact]
        public async Task GivenGetCategoryQuery_WhenIsExecuted_ThenEntityShouldBeReturned()
        {
            await using var provider = _serviceCollection.BuildServiceProvider();
            var getQuery = new GetCategoryQuery
            {
                Id = CategoryId
            };

            var mediator = provider.GetService<IMediator>();
            var classification = await mediator.Send<GetCategoryQuery, Category>(getQuery);

            Assert.NotNull(classification);
            Assert.Equal(CategoryId, classification.Id);
        }

        [Fact]
        public async Task GivenGetCategoryQuery_WhenEntityDoesNotExist_ThenExceptionShouldBeThrown()
        {
            await using var provider = _serviceCollection.BuildServiceProvider();
            var getQuery = new GetCategoryQuery
            {
                Id = 1006500
            };

            var mediator = provider.GetService<IMediator>();

            var exception = await Assert.ThrowsAsync<EntityNotFoundException>(() => mediator.Send<GetCategoryQuery, Category>(getQuery));

            Assert.Equal("Entity not found", exception.Message);
        }

        private void MockDbContext(IContext context)
        {
            var category = new Category
            {
                Name = "Test category"
            };

            context.Categories.Add(category);

            context.SaveChanges();
        }
    }
}
