using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Task2_2.Application.Interfaces;
using Task2_2.Application.Models;
using Task2_2.Application.UseCases.Categories.Query.GetCategories;
using Task2_2.Core.Interfaces;
using Task2_2.Domain;
using Xunit;

namespace Task2_2.Application.Tests.UseCases.Categories
{
    public class GetCategoriesQueryHandlerTests
    {
        private readonly IServiceCollection _serviceCollection;

        public GetCategoriesQueryHandlerTests()
        {
            _serviceCollection = new ServiceCollection();
            _serviceCollection.AddHandler<GetCategoriesQuery, PaginatedResult<Category>, GetCategoriesQueryHandler>(MockDbContext);
        }

        [Fact]
        public async Task GivenGetCategoriesQuery_WhenIsExecuted_ThenEntitiesPageShouldBeReturned()
        {
            await using var provider = _serviceCollection.BuildServiceProvider();
            var getAllQuery = new GetCategoriesQuery
            {
                PageSize = 2,
                StartPage = 1
            };

            var mediator = provider.GetService<IMediator>();
            var categoriesPageResult = await mediator.Send<GetCategoriesQuery, PaginatedResult<Category>>(getAllQuery);

            Assert.NotNull(categoriesPageResult);
            Assert.Equal(getAllQuery.StartPage, categoriesPageResult.Page);
            Assert.Equal(3, categoriesPageResult.Total);
            Assert.Equal(getAllQuery.PageSize, categoriesPageResult.Items.Count);
        }

        [Fact]
        public async Task GivenGetCategoriesQuery_WhenNotExistingPageRequested_ThenNoEntitiesPageShouldBeReturned()
        {
            await using var provider = _serviceCollection.BuildServiceProvider();
            var getAllQuery = new GetCategoriesQuery
            {
                PageSize = 2,
                StartPage = 10
            };

            var mediator = provider.GetService<IMediator>();
            var categoriesPageResult = await mediator.Send<GetCategoriesQuery, PaginatedResult<Category>>(getAllQuery);

            Assert.NotNull(categoriesPageResult);
            Assert.Equal(getAllQuery.StartPage, categoriesPageResult.Page);
            Assert.Equal(3, categoriesPageResult.Total);
            Assert.Empty(categoriesPageResult.Items);
        }

        private void MockDbContext(IContext context)
        {
            var category1 = new Category
            {
                Name = "Test category1"
            };

            var category2 = new Category
            {
                Name = "Test category2"
            };

            var category3 = new Category
            {
                Name = "Test category3"
            };

            context.Categories.AddRange(new List<Category> { category1, category2, category3 });

            context.SaveChanges();
        }
    }
}
