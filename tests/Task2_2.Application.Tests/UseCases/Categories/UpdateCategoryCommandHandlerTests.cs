﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Task2_2.Application.Exceptions;
using Task2_2.Application.Interfaces;
using Task2_2.Application.UseCases.Categories.Command.UpdateCategory;
using Task2_2.Core.Interfaces;
using Task2_2.Domain;
using Xunit;

namespace Task2_2.Application.Tests.UseCases.Categories
{
    public class UpdateCategoryCommandHandlerTests
    {
        private readonly IServiceCollection _serviceCollection;

        public UpdateCategoryCommandHandlerTests()
        {
            _serviceCollection = new ServiceCollection();
            _serviceCollection.AddCommandHandler<UpdateCategoryCommand, UpdateCategoryCommandHandler, Category>(MockDbContext);
        }

        [Fact]
        public async Task GivenUpdateCategoryCommand_WhenIsExecuted_ThenEntityShouldBeUpdatedInDatabase()
        {
            await using var provider = _serviceCollection.BuildServiceProvider();
            var updateCommand = new UpdateCategoryCommand
            {
                Name = "Test category_updates",
                Image = "test URL_updated",
                Id = 1
            };

            var mediator = provider.GetService<IMediator>();
            await mediator.Send<UpdateCategoryCommand, Category>(updateCommand);
            
            var updatedEntity = provider.GetService<IContext>().Categories.FirstOrDefault();

            Assert.NotNull(updatedEntity);
            Assert.Equal(updateCommand.Name, updatedEntity.Name);
            Assert.Equal(updateCommand.Image, updatedEntity.Image);
            Assert.Equal(updateCommand.ParentId, updatedEntity.ParentId);
            Assert.True(updatedEntity.Created < updatedEntity.Updated);
        }

        [Fact]
        public async Task GivenUpdateCategoryCommand_WhenEntityDoesNotExist_ThenExceptionShouldBeThrown()
        {
            await using var provider = _serviceCollection.BuildServiceProvider();
            var updateCommand = new UpdateCategoryCommand
            {
                Name = "Test category_updates",
                Image = "test URL_updated",
                Id = 100500
            };

            var mediator = provider.GetService<IMediator>();

            var exception = await Assert.ThrowsAsync<EntityNotFoundException>(() => mediator.Send<UpdateCategoryCommand, Category>(updateCommand));

            Assert.Equal("Entity not found", exception.Message);
        }

        private void MockDbContext(IContext context)
        {
            var category = new Category
            {
                Name = "Test category"
            };

            context.Categories.Add(category);

            context.SaveChanges();
        }
    }
}
