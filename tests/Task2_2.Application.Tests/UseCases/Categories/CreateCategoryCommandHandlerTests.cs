﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Task2_2.Application.Interfaces;
using Task2_2.Application.UseCases.Categories.Command.CreateCategory;
using Task2_2.Core.Interfaces;
using Task2_2.Domain;
using Xunit;

namespace Task2_2.Application.Tests.UseCases.Categories
{
    public class CreateCategoryCommandHandlerTests
    {
        private readonly IServiceCollection _serviceCollection;

        public CreateCategoryCommandHandlerTests()
        {
            _serviceCollection = new ServiceCollection();
            _serviceCollection.AddCommandHandler<CreateCategoryCommand, CreateCategoryCommandHandler, int>(MockDbContext);
        }

        [Fact]
        public async Task GivenCreateCategoryCommand_WhenIsExecuted_ThenEntityShouldBeCreatedInDatabase()
        {
            await using var provider = _serviceCollection.BuildServiceProvider();
            var createCommand = new CreateCategoryCommand
            {
                Name = "Test category",
                Image = "test URL",
                ParentId = 1
            };

            var mediator = provider.GetService<IMediator>();
            await mediator.Send<CreateCategoryCommand, int>(createCommand);
            
            var createdEntity = provider.GetService<IContext>().Categories.LastOrDefault();

            Assert.NotNull(createdEntity);
            Assert.Equal(createCommand.Name, createdEntity.Name);
            Assert.Equal(createCommand.Image, createdEntity.Image);
            Assert.Equal(createCommand.ParentId, createdEntity.ParentId);
        }

        private void MockDbContext(IContext context)
        {
            var category = new Category
            {
                Name = "Test category"
            };

            context.Categories.Add(category);

            context.SaveChanges();
        }
    }
}
