using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Task2_2.Application.Exceptions;
using Task2_2.Application.Interfaces;
using Task2_2.Application.UseCases.Items.Query.GetItem;
using Task2_2.Core.Interfaces;
using Task2_2.Domain;
using Xunit;

namespace Task2_2.Application.Tests.UseCases.Items
{
    public class GetItemQueryHandlerTests
    {
        private const int ItemId = 1;

        private readonly IServiceCollection _serviceCollection;

        public GetItemQueryHandlerTests()
        {
            _serviceCollection = new ServiceCollection();
            _serviceCollection.AddHandler<GetItemQuery, Item, GetItemQueryHandler>(MockDbContext);
        }

        [Fact]
        public async Task GivenGetItemQuery_WhenIsExecuted_ThenEntityShouldBeReturned()
        {
            await using var provider = _serviceCollection.BuildServiceProvider();
            var getQuery = new GetItemQuery
            {
                Id = ItemId
            };

            var mediator = provider.GetService<IMediator>();
            var classification = await mediator.Send<GetItemQuery, Item>(getQuery);

            Assert.NotNull(classification);
            Assert.NotNull(classification.Parent);
            Assert.Equal(ItemId, classification.Id);
        }

        [Fact]
        public async Task GivenGetItemQuery_WhenEntityDoesNotExist_ThenExceptionShouldBeThrown()
        {
            await using var provider = _serviceCollection.BuildServiceProvider();
            var getQuery = new GetItemQuery
            {
                Id = 1006500
            };

            var mediator = provider.GetService<IMediator>();

            var exception = await Assert.ThrowsAsync<EntityNotFoundException>(() => mediator.Send<GetItemQuery, Item>(getQuery));

            Assert.Equal("Entity not found", exception.Message);
        }

        private void MockDbContext(IContext context)
        {
            var category = new Category
            {
                Name = "Test category"
            };

            context.Categories.Add(category);

            context.SaveChanges();

            var item = new Item
            {
                Name = "Test Item",
                ParentId = 1
            };

            context.Items.Add(item);

            context.SaveChanges();
        }
    }
}
