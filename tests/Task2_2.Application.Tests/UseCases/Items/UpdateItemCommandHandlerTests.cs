﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Task2_2.Application.Exceptions;
using Task2_2.Application.Interfaces;
using Task2_2.Application.UseCases.Items.Command.UpdateItem;
using Task2_2.Core.Interfaces;
using Task2_2.Domain;
using Task4.ServiceBusClient.Contracts;
using Xunit;

namespace Task2_2.Application.Tests.UseCases.Items
{
    public class UpdateItemCommandHandlerTests
    {
        private readonly IServiceCollection _serviceCollection;
        private readonly Mock<IServiceBusSender> _serviceBusServiceMock;

        public UpdateItemCommandHandlerTests()
        {
            _serviceCollection = new ServiceCollection();
            _serviceBusServiceMock = new Mock<IServiceBusSender>();
            _serviceCollection.AddSingleton(p => _serviceBusServiceMock.Object);
            _serviceCollection.AddCommandHandler<UpdateItemCommand, UpdateItemCommandHandler, Item>(MockDbContext);
        }

        [Fact]
        public async Task GivenUpdateItemCommand_WhenIsExecuted_ThenEntityShouldBeUpdatedInDatabase()
        {
            await using var provider = _serviceCollection.BuildServiceProvider();
            var updateCommand = new UpdateItemCommand
            {
                Name = "Test Item_updated",
                Amount = 200,
                Description = "Test description_updated",
                Image = "test URL_updated",
                ParentId = 1,
                Price = 2000,
                Id = 1
            };

            var mediator = provider.GetService<IMediator>();
            await mediator.Send<UpdateItemCommand, Item>(updateCommand);
            
            var updatedEntity = provider.GetService<IContext>().Items.FirstOrDefault();

            Assert.NotNull(updatedEntity);
            Assert.Equal(updateCommand.Name, updatedEntity.Name);
            Assert.Equal(updateCommand.Image, updatedEntity.Image);
            Assert.Equal(updateCommand.ParentId, updatedEntity.ParentId);
            Assert.Equal(updateCommand.Description, updatedEntity.Description);
            Assert.Equal(updateCommand.Amount, updatedEntity.Amount);
            Assert.Equal(updateCommand.Price, updatedEntity.Price);
            Assert.True(updatedEntity.Created < updatedEntity.Updated);

            _serviceBusServiceMock.Verify(x => x.SendMessage(It.IsAny<string>()), Times.Once());
        }

        [Fact]
        public async Task GivenUpdateItemCommand_WhenEntityDoesNotExist_ThenExceptionShouldBeThrown()
        {
            await using var provider = _serviceCollection.BuildServiceProvider();
            var updateCommand = new UpdateItemCommand
            {
                Name = "Test Item_updated",
                Amount = 200,
                Description = "Test description_updated",
                Image = "test URL_updated",
                ParentId = 1,
                Price = 2000,
                Id = 100500
            };

            var mediator = provider.GetService<IMediator>();

            var exception = await Assert.ThrowsAsync<EntityNotFoundException>(() => mediator.Send<UpdateItemCommand, Item>(updateCommand));

            Assert.Equal("Entity not found", exception.Message);
        }

        private void MockDbContext(IContext context)
        {
            var category = new Category
            {
                Name = "Test category"
            };

            context.Categories.Add(category);

            context.SaveChanges();
            var item = new Item
            {
                Name = "Test Item",
                Amount = 10,
                Description = "Test description",
                Image = "test URL",
                ParentId = 1,
                Price = 20
            };

            context.Items.Add(item);

            context.SaveChanges();
        }
    }
}
