﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Task2_2.Application.Exceptions;
using Task2_2.Application.Interfaces;
using Task2_2.Application.UseCases.Items.Command.DeleteItem;
using Task2_2.Core.Interfaces;
using Task2_2.Domain;
using Task4.ServiceBusClient.Contracts;
using Xunit;

namespace Task2_2.Application.Tests.UseCases.Items
{
    public class DeleteItemCommandHandlerTests
    {
        private readonly IServiceCollection _serviceCollection;
        private readonly Mock<IServiceBusSender> _serviceBusServiceMock;

        public DeleteItemCommandHandlerTests()
        {
            _serviceCollection = new ServiceCollection();
            _serviceBusServiceMock = new Mock<IServiceBusSender>();
            _serviceCollection.AddSingleton(p => _serviceBusServiceMock.Object);
            _serviceCollection.AddCommandHandler<DeleteItemCommand, DeleteItemCommandHandler, int>(MockDbContext);
        }

        [Fact]
        public async Task GivenDeleteItemCommand_WhenIsExecuted_ThenEntityShouldBeDeletedInDatabase()
        {
            await using var provider = _serviceCollection.BuildServiceProvider();
            var deleteCommand = new DeleteItemCommand
            {
                Id = 1
            };

            var initialEntitiesNumber = provider.GetService<IContext>().Items.Count();

            var mediator = provider.GetService<IMediator>();
            await mediator.Send<DeleteItemCommand, int>(deleteCommand);
            
            var actualEntitiesNumber = provider.GetService<IContext>().Items.Count();
            var entity = provider.GetService<IContext>().Items.FirstOrDefault(x => x.Id == deleteCommand.Id);

            Assert.Null(entity);
            Assert.Equal(initialEntitiesNumber, actualEntitiesNumber + 1);

            _serviceBusServiceMock.Verify(x => x.SendMessage(It.IsAny<string>()), Times.Once());
        }

        [Fact]
        public async Task GivenDeleteItemCommand_WhenEntityDoesNotExist_ThenExceptionShouldBeThrown()
        {
            await using var provider = _serviceCollection.BuildServiceProvider();
            var deleteCommand = new DeleteItemCommand
            {
                Id = 100500
            };

            var initialEntitiesNumber = provider.GetService<IContext>().Items.Count();

            var mediator = provider.GetService<IMediator>();

            var actualEntitiesNumber = provider.GetService<IContext>().Items.Count();
            var exception = await Assert.ThrowsAsync<EntityNotFoundException>(() => mediator.Send<DeleteItemCommand, int>(deleteCommand));

            Assert.Equal("Entity not found", exception.Message);
            Assert.Equal(initialEntitiesNumber, actualEntitiesNumber);
        }

        private void MockDbContext(IContext context)
        {
            var category = new Category
            {
                Name = "Test category"
            };

            context.Categories.Add(category);

            context.SaveChanges();

            var item = new Item
            {
                Name = "Test Item",
                Amount = 10,
                Description = "Test description",
                Image = "test URL",
                ParentId = 1,
                Price = 20
            };

            context.Items.Add(item);

            context.SaveChanges();
        }
    }
}
