﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Task2_2.Application.Interfaces;
using Task2_2.Application.UseCases.Items.Command.CreateItem;
using Task2_2.Core.Interfaces;
using Task2_2.Domain;
using Xunit;

namespace Task2_2.Application.Tests.UseCases.Items
{
    public class CreateItemCommandHandlerTests
    {
        private readonly IServiceCollection _serviceCollection;

        public CreateItemCommandHandlerTests()
        {
            _serviceCollection = new ServiceCollection();
            _serviceCollection.AddCommandHandler<CreateItemCommand, CreateItemCommandHandler, int>(MockDbContext);
        }

        [Fact]
        public async Task GivenCreateItemCommand_WhenIsExecuted_ThenEntityShouldBeCreatedInDatabase()
        {
            await using var provider = _serviceCollection.BuildServiceProvider();
            var createCommand = new CreateItemCommand
            {
                Name = "Test Item",
                Amount = 10,
                Description = "Test description",
                Image = "test URL",
                ParentId = 1,
                Price = 20
            };

            var mediator = provider.GetService<IMediator>();
            await mediator.Send<CreateItemCommand, int>(createCommand);
            
            var createdEntity = provider.GetService<IContext>().Items.LastOrDefault();

            Assert.NotNull(createdEntity);
            Assert.Equal(createCommand.Name, createdEntity.Name);
            Assert.Equal(createCommand.Image, createdEntity.Image);
            Assert.Equal(createCommand.ParentId, createdEntity.ParentId);
            Assert.Equal(createCommand.Description, createdEntity.Description);
            Assert.Equal(createCommand.Price, createdEntity.Price);
        }

        private void MockDbContext(IContext context)
        {
            var category = new Category
            {
                Name = "Test category"
            };

            context.Categories.Add(category);

            context.SaveChanges();
        }
    }
}
