using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Task2_2.Application.Interfaces;
using Task2_2.Application.Models;
using Task2_2.Application.UseCases.Items.Query.GetItems;
using Task2_2.Core.Interfaces;
using Task2_2.Domain;
using Xunit;

namespace Task2_2.Application.Tests.UseCases.Items
{
    public class GetItemsQueryHandlerTests
    {
        private readonly IServiceCollection _serviceCollection;

        public GetItemsQueryHandlerTests()
        {
            _serviceCollection = new ServiceCollection();
            _serviceCollection.AddHandler<GetItemsQuery, PaginatedResult<Item>, GetItemsQueryHandler>(MockDbContext);
        }

        [Fact]
        public async Task GivenGetItemsQuery_WhenIsExecuted_ThenEntitiesPageShouldBeReturned()
        {
            await using var provider = _serviceCollection.BuildServiceProvider();
            var getAllQuery = new GetItemsQuery
            {
                PageSize = 2,
                StartPage = 1
            };

            var mediator = provider.GetService<IMediator>();
            var itemsPageResult = await mediator.Send<GetItemsQuery, PaginatedResult<Item>>(getAllQuery);

            Assert.NotNull(itemsPageResult);
            Assert.Equal(getAllQuery.StartPage, itemsPageResult.Page);
            Assert.Equal(3, itemsPageResult.Total);
            Assert.Equal(getAllQuery.PageSize, itemsPageResult.Items.Count);
        }

        [Fact]
        public async Task GivenGetItemsQuery_WhenNotExistingPageRequested_ThenNoEntitiesPageShouldBeReturned()
        {
            await using var provider = _serviceCollection.BuildServiceProvider();
            var getAllQuery = new GetItemsQuery
            {
                PageSize = 2,
                StartPage = 10
            };

            var mediator = provider.GetService<IMediator>();
            var itemsPageResult = await mediator.Send<GetItemsQuery, PaginatedResult<Item>>(getAllQuery);

            Assert.NotNull(itemsPageResult);
            Assert.Equal(getAllQuery.StartPage, itemsPageResult.Page);
            Assert.Equal(3, itemsPageResult.Total);
            Assert.Empty(itemsPageResult.Items);
        }

        private void MockDbContext(IContext context)
        {
            var category = new Category
            {
                Name = "Test category"
            };

            context.Categories.Add(category);

            context.SaveChanges();

            var item1 = new Item
            {
                Name = "Test Item1",
                ParentId = 1
            };

            var item2 = new Item
            {
                Name = "Test Item2",
                ParentId = 1
            };

            var item3 = new Item
            {
                Name = "Test Item3",
                ParentId = 1
            };

            context.Items.AddRange(new List<Item> { item1, item2, item3 });

            context.SaveChanges();
        }
    }
}
