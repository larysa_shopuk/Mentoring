﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Task2_2.Application.AutoMapperProfiles;
using Task2_2.Application.Interfaces;
using Task2_2.Core;
using Task2_2.Core.Interfaces;

namespace Task2_2.Application.Tests
{
    public static class HandlerTestExtensions
    {
        public static IServiceCollection AddCommandHandler<TCommand, THandler, TResponse>(this IServiceCollection serviceCollection, Action<IContext> db = null)
            where THandler : class, IRequestHandler<TCommand, TResponse>
            where TCommand : IRequest<TResponse>
        {
            return serviceCollection
                .AddHandler<TCommand, TResponse, THandler>(db);
        }

        public static IServiceCollection AddHandler<TCommand, TResponse, THandler>(this IServiceCollection serviceCollection, Action<IContext> db = null)
            where THandler : class, IRequestHandler<TCommand, TResponse>
            where TCommand : IRequest<TResponse> => serviceCollection
                .AddSingleton<IContext>(p =>
                {
                    var context = TestDbContextFactory.CreateInMemory();
                    db?.Invoke(context);
                    return context;
                })
                .AddAutoMapper(typeof(AutoMapperProfile).Assembly)
                .AddTransient<IMediator, Mediator>()
                .AddTransient<IHandlerProvider>(p => new HandlerProvider(p))
                .AddTransient<IRequestHandler<TCommand, TResponse>, THandler>();
    }
}
